#Servcorp	Meetings Server

### What is this repo?

This repo is the server side of the Meetings app. The other two Repo's [Android](https://github.com/engagingcomms/Servcorp-Meetings-Android-App) and [iOS](https://github.com/engagingcomms/Servcorp_Meetings_IOS) are only simple shell apps that point to the server. 

#####Server urls

[English Server](http://en.servcorponline.com)

[Japanese Server](http://jp.servcorponline.com)
	
### Location
This is located on AWS Server.
Current Address as of writing this document is `175.41.132.33` You access this server via adding the pem file to your computer then accessing it either via transmit or another program.

#### File location
The files are located at 

English server - `/data/home/servcorp/production/releases/20120517/Frontend/Zend/`

Japanese Server - `/data/home/servcorpjp/production/releases/20120517/Frontend/Zend/`


*Please note that the server location is currently within the folder 20120517. As I do not know how the system is configured I just update this folder with the updated files you need to change*
