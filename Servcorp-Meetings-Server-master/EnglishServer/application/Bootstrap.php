<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
	public function run()
	{
		set_include_path('../library' . PATH_SEPARATOR . get_include_path());
		require_once 'Zend/Controller/Front.php';
		require_once 'Zend/Controller/Router/Route.php';
		require_once 'controllers/BaseController.php';
		require_once 'SOAPAuth.php';

		$config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', APPLICATION_ENV);
		
		$this->bootstrap('db');
		$this->baseurl = $this->view->baseurl = $config->url->baseUrl;
		
		$sessionConfig = array('name'=> 'website_session',
								'primary'=> array('SessionId','SavePath','Name'),
								'primaryAssignment' => array(
									'sessionId', //first column of the primary key is of the sessionID
									'sessionSavePath', //second column of the primary key is the save path
									'sessionName', //third column of the primary key is the session name
									),
								'modifiedColumn'=> 'Modified',//time the session should expire
								'dataColumn'=> 'SessionData', //serialized data
								'lifetimeColumn'=> 'Lifetime',//end of life for a specific record
								);
		$savehandler = new Zend_Session_SaveHandler_DbTable($sessionConfig);
		
		$savehandler->setLifetime(60 * 60 * 24 * 30)->setOverrideLifetime(true); 
		Zend_Session::setSaveHandler($savehandler);
		
		Zend_Registry::set('soapOptions', array("login"=>$config->soap->login,"password"=>$config->soap->password));
		$ctrl  = Zend_Controller_Front::getInstance();
		$ctrl->setParam('bootstrap', $this);
		$router = $ctrl->getRouter();
		//$router->addRoute('login', new Zend_Controller_Router_Route('login',array('controller' => 'login','action'  => 'index')));
		$ctrl->run('../application/controllers');
	}
	
    protected function _initDoctype()
    {
        $this->bootstrap('view');
        $view = $this->getResource('view');
        $view->doctype('XHTML1_STRICT');
    }
    
    protected function _initLogger()
    {
    	$stream = fopen('../public/log/debug_log', 'a', false);
    
    	$writer = new Zend_Log_Writer_Stream($stream);
    	$logger = new Zend_Log($writer);
    	Zend_Registry::set('logger', $logger);
    }
    
    protected function _initCustomClassPath()
    {
    	$this->getResourceLoader()->addResourceType('servcorp_utils','utils/','UTILS');
    	$this->getResourceLoader()->addResourceType('misc','misc/','Misc');
    }
    
    protected function _initMinifyScript()
    {
    	$this->bootstrap('view');
    	$view = $this->getResource('view');
    	$minifyScript = new Misc_View_Helper_MinifyScript();
    	$view->registerHelper($minifyScript,"minifyScript");
    	$view->headScript()->appendFile('js/jquery-1.6.4.js');
    	$view->headScript()->appendFile('js/jquery-ui-1.8.2.custom.min.js');
    	$view->headScript()->appendFile('js/jquery.mobile-1.0.1.js');
    	$view->headScript()->appendFile('js/servcorpajax.js');
    }
    
	protected function _initLanguages() {
		error_reporting(E_ALL);
		ini_set('display_errors', 'On');
		require_once 'utils/Translator.php';
		$translater = new Translator();
		$view = $this->getResource('view');
		$view->translater=$translater;
	}
	
//	protected function _initTimeZone()
//	{
//		date_default_timezone_set('Asia/Tokyo');
//		setlocale(LC_ALL, "ja_JP");
//		//$locale = Zend_Registry::get('Zend_Locale');
////		echo '<pre>';print_r($locale);die();
////		$locale = new Zend_Locale('ja_JP');
////
////		$date = new Zend_Date( strtotime('yesterday'), null, $locale);
////		echo $date->get(Zend_Date::DATE_LONG);die();
//
//
//	}
}

