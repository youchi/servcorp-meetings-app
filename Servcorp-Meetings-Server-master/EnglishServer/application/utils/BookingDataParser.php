<?php
class UTILS_BookingDataParser {
	protected $dataSet;
	public function __construct() {
		$this->xml_head = '<?xml version="1.0"?>';
		$soap=new Model_SOAPCalls();
		$xm=$soap->getBookedResource();
		$xmlObj = new Zend_Config_Xml($this->xml_head.$xm);
		$this->dataSet = $xmlObj->HottdeskResourceDataSet;
	}

	public function insertCountry() {
		$country_arr = array();
		$id='tblCountry';
		$i=1;
		//To create a table if not exists
		$query="CREATE TABLE IF NOT EXISTS tblCountry (_ID INT NOT NULL PRIMARY KEY AUTO_INCREMENT,_IDNewDataSet char(1) default '1',CalcDay char(2),
                CountryCode char(2),CultureCode char(5),CurrencyCode char(3),DefaultLanguage char(1),Description char(20),
                 id char(12),InterestRate char(1),PortalCode char(1),rowOrder char(2),TaxOn char(5))";
		$c=new Model_Country();
		$c->getAdapter()->query($query);
		$c->getAdapter()->query("delete from tblCountry");
		if (count($this->dataSet->tblCountry)!=0)
		foreach ($this->dataSet->tblCountry as $country) {
			$row=$c->select()->where('CountryCode=?',$country->CountryCode);
			$test=$c->fetchAll($row)->toArray();
			if(empty($test)){
				$country_arr = array('CalcDay'=>$country->CalcDay,'CountryCode'=>$country->CountryCode,'CultureCode'=>$country->CultureCode,
													'CurrencyCode'=>$country->CurrencyCode,'DefaultLanguage'=>$country->DefaultLanguage,
													'Description'=>$country->Description,'id'=>$id.$i,'InterestRate'=>$country->InterestRate,
													'PortalCode'=>$country->PortalCode,'rowOrder'=>($i-1),'TaxOn'=>$country->TaxOn);
				$ins=$c->insert($country_arr) ;
			}
		 $i++;
		}
	}


	public function insertCity() {
		$c_arr = array();
		$id='tblCity';
		$i=1;
		//To create a table if not exists
		$query="CREATE TABLE IF NOT EXISTS tblCity (_ID INT NOT NULL PRIMARY KEY AUTO_INCREMENT,_IDNewDataSet char(1) default '1',
              CityCode char(4),CountryCode char(2),Description char(12),id char(9),rowOrder char(2),TaxRegionCode char(2))";
		$c=new Model_City();
		$c->getAdapter()->query($query);
		$c->getAdapter()->query("delete from tblCity");
		if (count($this->dataSet->tblCity)!=0)
		foreach ($this->dataSet->tblCity as $city) {
		 $row=$c->select()->where('CityCode=?',$city->CityCode);
		 $test=$c->fetchAll($row)->toArray();
		 if(empty($test)){
		 	$c_arr = array('CityCode'=>$city->CityCode,'CountryCode'=>$city->CountryCode,
											'Description'=>$city->Description,'id'=>$id.$i,
											'rowOrder'=>($i-1),'TaxRegionCode'=>$city->TaxRegionCode);
		 	$c->insert($c_arr) ;
		 }
		 $i++;
		}
	}
	public function insertLocation() {
		$loc_arr = array();
		$id='tblLocation';
		$i=1;
		//To create a table if not exists
		$query="CREATE TABLE IF NOT EXISTS tblLocation (_ID INT NOT NULL PRIMARY KEY AUTO_INCREMENT,_IDNewDataSet char(1),AltDescription char(39),
                CityCode char(4),Description char(40),id char(13),LocationCode char(3),ProxyURL char(81),HorizontalPosition char(40),VerticalPosition char(40),rowOrder char(2))";
		$c=new Model_Location();
		$c->getAdapter()->query($query);
		$c->getAdapter()->query("delete from tblLocation");
		if (count($this->dataSet->tblLocation)!=0)
		{
			$insqry="INSERT into tblLocation (AltDescription,CityCode,Description,id,LocationCode,ProxyURL,HorizontalPosition,VerticalPosition,rowOrder) values ";
			foreach ($this->dataSet->tblLocation as $country){
				$insqry.="('".$country->AltDescription."','".$country->CityCode."','".$country->Description."','".$id.$i."','".$country->LocationCode."','".$country->ProxyURL."','".$country->HorizontalPosition."','".$country->VerticalPosition."','".($i-1)."'),";
				$i++;
			}
			$c->getAdapter()->query(chop($insqry,','));
		}
	}
	public function insertResource() {
		$res_arr = array();
		$id='tblResource';
		$i=1;
		//To create a Table if not exists
		$query="CREATE TABLE IF NOT EXISTS tblResource (_ID INT NOT NULL PRIMARY KEY AUTO_INCREMENT,_IDNewDataSet char(1) default '1',Complimentable char(5),
		GroupID char(1),id char(15),ItemIDAfterHour char(8),ItemIDAfterMin char(8),ItemIDComp char(8),ItemIDFlatRate char(8),
        ItemIDUnit char(8),ItemIDWeekEnd char(8),ResourceID char(4),ResourceName char(100),ResourceTypeID char(1),rowOrder char(4),
        SiteID char(5),Status char(1),Username char(15))";
		$c=new Model_Resource();
		$c->getAdapter()->query($query);
		$c->getAdapter()->query("delete from tblResource");
		if (count($this->dataSet->tblResource)!=0)
		foreach ($this->dataSet->tblResource as $res) {
		 $row=$c->select()->where('ResourceID=?',$res->ResourceID);
		 $test=$c->fetchAll($row)->toArray();
		 if(empty($test)){
		 	$res_arr = array('Complimentable'=>$res->Complimentable,'GroupID'=>$res->GroupID,'id'=>$id.$i,
											'ItemIDAfterHour'=>$res->ItemIDAfterHour,'ItemIDAfterMin'=>$res->ItemIDAfterMin,
											'ItemIDComp'=>$res->ItemIDComp,'ItemIDFlatRate'=>$res->ItemIDFlatRate,
											'ItemIDUnit'=>$res->ItemIDUnit,'ItemIDWeekEnd'=>$res->ItemIDWeekEnd,'ResourceID'=>$res->ResourceID,
											'ResourceName'=>$res->ResourceName,'ResourceTypeID'=>$res->ResourceTypeID,'rowOrder'=>($i-1),
											'SiteID'=>$res->SiteID,'Status'=>$res->Status,'Username'=>$res->Username);
		 	$c->insert($res_arr) ;
		 }
		 $i++;
		}
	}
	public function insertResourceType() {
		$rt_arr = array();
		$id='tblResourceType';
		$i=1;
		//To create a table if not exists
		$query="CREATE TABLE IF NOT EXISTS tblResourceType (_ID INT NOT NULL PRIMARY KEY AUTO_INCREMENT,_IDNewDataSet char(1) default '1',
		        Description char(25),id char(16),ResourceTypeID char(1),rowOrder char(1),SecretaryService char(5))";
		$c=new Model_ResourceType();
		$c->getAdapter()->query($query);
		$c->getAdapter()->query("delete from tblResourceType");
		if (count($this->dataSet->tblResourceType)!=0)
		foreach ($this->dataSet->tblResourceType as $rt) {
		 $row=$c->select()->where('ResourceTypeID=?',$rt->ResourceTypeID);
		 $test=$c->fetchAll($row)->toArray();
		 if(empty($test)){
		 	$rt_arr = array('Description'=>$rt->Description,'id'=>$id.$i,'ResourceTypeID'=>$rt->ResourceTypeID,'rowOrder'=>($i-1),
								'SecretaryService'=>$rt->SecretaryService,);
		 	$c->insert($rt_arr) ;
		 }
		 $i++;
		}
	}
	public function insertSite() {
		$s_arr = array();
		$id='tblSite';
		$i=1;
		//To create a table if not exists
		$query="CREATE TABLE IF NOT EXISTS tblSite (_ID INT NOT NULL PRIMARY KEY AUTO_INCREMENT,_IDNewDataSet char(1) default '1',AccountName char(58),
                AccountNo char(26),AccountSummary char(5),AccountType char(29),AddressLine1 char(55),AddressLine2 char(56),AddressLine3 char(36),
                AltAccountName char(52),AltAccountType char(29),AltAddressLine1 char(42),AltAddressLine2 char(56),AltAddressLine3 char(42),
                AltBankName char(56),AltBranchName char(64),AltDescription char(44),AltInvoiceNote char(33),AltSiteName char(42),
                BankName char(56),BranchName char(64),BranchNumber char(20),BSBNumber char(20),CallBilling char(5),CallBillingLimit char(12),
                CommonSpace char(8),CompanyID char(10),Description char(45),Email char(40),Fax char(17),HottdeskCode char(3),
                HottdeskVersion char(3),id char(10),InternetBilling char(5),InternetBillingLimit char(12),InvoiceNote char(69),LocationCode char(3),
                MerchantID char(13),Phone char(17),PostCode char(8),ReportType char(1),rowOrder char(3),ServcorpSpace char(1),
                SiteID char(5),SiteName char(46),SiteType char(1),SwiftCode char(25),TaxOnTotal char(5),TerminalID char(8),
                WebPage char(26),tblSite_space char(8))";
		$c=new Model_Site();
		$c->getAdapter()->query($query);
		$c->getAdapter()->query("delete from tblSite");
		if (count($this->dataSet->tblSite)!=0)
		foreach ($this->dataSet->tblSite as $s) {
		 $row=$c->select()->where('SiteID=?',$s->SiteID);
		 $test=$c->fetchAll($row)->toArray();
		 if(empty($test)){
		 	$s_arr = array('AccountName'=>$s->AccountName,'AccountNo'=>$s->AccountNo,
							'AccountSummary'=>$s->AccountSummary,'AccountType'=>$s->AccountType,'AddressLine1'=>$s->AddressLine1,
							'AddressLine2'=>$s->AddressLine2,'AddressLine3'=>$s->AddressLine3,'AltAccountName'=>$s->AltAccountName,
							'AltAccountType'=>$s->AltAccountType,'AltAddressLine1'=>$s->AltAddressLine1,'AltAddressLine2'=>$s->AltAddressLine2,
							'AltAddressLine3'=>$s->AltAddressLine3,'AltBankName'=>$s->AltBankName,'AltBranchName'=>$s->AltBranchName,
							'AltDescription'=>$s->AltDescription,'AltInvoiceNote'=>$s->AltInvoiceNote,'AltSiteName'=>$s->AltSiteName,
							'BankName'=>$s->BankName,'BranchName'=>$s->BranchName,'BranchNumber'=>$s->BranchNumber,
							'BSBNumber'=>$s->BSBNumber,'CallBilling'=>$s->CallBilling,'CallBillingLimit'=>$s->CallBillingLimit,
							'CommonSpace'=>$s->CommonSpace,'CompanyID'=>$s->CompanyID,'Description'=>$s->Description,
							'Email'=>$s->Email,'Fax'=>$s->Fax,'HottdeskCode'=>$s->HottdeskCode,'HottdeskVersion'=>$s->HottdeskVersion,
							'id'=>$id.$i,'InternetBilling'=>$s->InternetBilling,'InternetBillingLimit'=>$s->InternetBillingLimit,
							'InvoiceNote'=>$s->InvoiceNote,'LocationCode'=>$s->LocationCode,'MerchantID'=>$s->MerchantID,
							'Phone'=>$s->Phone,'PostCode'=>$s->PostCode,'ReportType'=>$s->ReportType,'rowOrder'=>($i-1),
							'ServcorpSpace'=>$s->ServcorpSpace,'SiteID'=>$s->SiteID,'SiteName'=>$s->SiteName,'SiteType'=>$s->SiteType,
							'SwiftCode'=>$s->SwiftCode,'TaxOnTotal'=>$s->TaxOnTotal,'TerminalID'=>$s->TerminalID,
							'WebPage'=>$s->WebPage,'tblSite_space'=>$s->tblSite_space);
		 	$c->insert($s_arr) ;
		 }
		 $i++;
		}
	}
	public function insertHoliday(){
		$h_arr=array();
		$id='tblBookingHolidayRule';
		$i=1;
		//To create a table if not exists
		$query="CREATE TABLE IF NOT EXISTS tblBookingHolidayRule (_ID INT NOT NULL PRIMARY KEY AUTO_INCREMENT,_IDNewDataSet char(1) default '1',
				CloseAllDay char(4),CloseDay char(2),CloseHour char(2),CloseMinute char(2),CloseMonth char(2),CloseYear char(4),
				id char(25),Note char(50),OpenHour char(2),OpenMinute char(2),rowOrder char(4),SiteID char(5))";
		$h=new Model_Holiday();
		$h->getAdapter()->query($query);
		$h->getAdapter()->query("delete from tblBookingHolidayRule");
		if (count($this->dataSet->tblBookingHolidayRule)!=0)
		foreach ($this->dataSet->tblBookingHolidayRule as $hy){
			$row=$h->select()->where('rowOrder=?',($i-1));
			$test=$h->fetchAll($row)->toArray();
			if(empty($test)){
				$h_arr=array('_IDNewDataSet'=>1,'CloseAllDay'=>$hy->CloseAllDay,'CloseDay'=>$hy->CloseDay,'CloseHour'=>$hy->CloseHour,
				'CloseMinute'=>$hy->CloseMinute,'CloseMonth'=>$hy->CloseMonth,'CloseYear'=>$hy->CloseYear,'id'=>$id.$i,
				'Note'=>$hy->Note,'OpenHour'=>$hy->OpenHour,'OpenMinute'=>$hy->OpenMinute,'rowOrder'=>($i-1),'SiteID'=>$hy->SiteID);
				$h->insert($h_arr);
			}
			$i++;
		}

	}
	public function insertWeekly(){
		$w_arr=array();
		$id='tblBookingHolidayRule';
		$i=1;
		//To create a table if not exists
		$query="CREATE TABLE IF NOT EXISTS tblBookingWeeklyRule (_ID INT NOT NULL PRIMARY KEY AUTO_INCREMENT,_IDNewDataSet char(1) default '1',
				AfterHour char(2),AfterMinute char(2),CloseAllDay char(5),CloseHour char(2),CloseMinute char(2),DayOfWeek char(1),
				id char(24),OpenHour char(1),OpenMinute char(2),rowOrder char(4),SiteID char(5))";
		$w=new Model_Weekly();
		$w->getAdapter()->query($query);
		$w->getAdapter()->query("delete from tblBookingWeeklyRule");
		if (count($this->dataSet->tblBookingWeeklyRule)!=0)
		foreach ($this->dataSet->tblBookingWeeklyRule as $wy){
			$row=$w->select()->where('rowOrder=?',($i-1));
			$test=$w->fetchAll($row)->toArray();
			if(empty($test)){
				$w_arr=array('_IDNewDataSet'=>1,'AfterHour'=>$wy->AfterHour,'AfterMinute'=>$wy->AfterMinute,'CloseAllDay'=>$wy->CloseAllDay,
				'CloseHour'=>$wy->CloseHour,'CloseMinute'=>$wy->CloseMinute,'DayOfWeek'=>$wy->DayOfWeek,'id'=>$id.$i,
				'OpenHour'=>$wy->OpenHour,'OpenMinute'=>$wy->OpenMinute,'rowOrder'=>($i-1),'SiteID'=>$wy->SiteID);
				$w->insert($w_arr);
			}
			$i++;
		}
	}


	public function getLocation($country='') {
		$loc_arr = array();
		$i = 0;
		foreach ($this->dataSet->tblLocation as $loc) {
			if ($country)
			foreach ($this->getCity($country) as $ci)
			if ($loc->CityCode!=$ci['CityCode'])
			continue;
			$loc_arr['location'.$i] = array('LocationCode'=>$loc->LocationCode,'LocationName'=>$loc->Description,'CityCode'=>$loc->CityCode);
			$i++;
		}
		return $loc_arr;
	}
	public function getLocationFromResource($resourceId) {
		$siteId=$loc=$city=$country=$phone=$fax='';
		foreach ($this->dataSet->tblResource as $res)
		if($res->ResourceID==$resourceId)
		$siteId=$res->SiteID;
		foreach ($this->dataSet->tblSite as $site)
		if ($site->SiteID==$siteId) {
			$loc=$site->LocationCode;
			$phone=$site->Phone;
			$fax=$site->Fax;
		}
		foreach ($this->dataSet->tblLocation as $lo)
		if ($lo->LocationCode==$loc) {
			$loc=$lo->Description;
			$city=$lo->CityCode;
		}
		foreach ($this->dataSet->tblCity as $ci)
		if ($ci->CityCode==$city) {
			$city=$ci->Description;
			$country=$ci->CountryCode;
		}
		foreach ($this->dataSet->tblCountry as $co)
		if ($co->CountryCode==$country)
		$country=$co->Description;
		return array('country'=>$country,'city'=>$city,'location'=>$loc,'phone'=>$phone,'fax'=>$fax);
	}
	public function inserttblHottdeskService(){
		$h_arr=array();
		$id='tblHottdeskService';
		$i=1;
		//To create a table if not exists
		$query="CREATE TABLE IF NOT EXISTS tblHottdeskService (_ID INT NOT NULL PRIMARY KEY AUTO_INCREMENT,_IDNewDataSet char(1) default '1',
				ActionLinkCode char(1),Description char(28),id char(20),IsBasicService char(5),ItemID char(8),
				rowOrder char(2),ServiceID char(2))";
		$h=new Model_HottdeskService();
		$h->getAdapter()->query($query);
		$h->getAdapter()->query("delete from tblHottdeskService");
		if (count($this->dataSet->tblHottdeskService)!=0)
		foreach ($this->dataSet->tblHottdeskService as $hy){
			$row=$h->select()->where('rowOrder=?',($i-1));
			$test=$h->fetchAll($row)->toArray();
			if(empty($test)){
				$h_arr=array('ActionLinkCode'=>$hy->ActionLinkCode,'Description'=>$hy->Description,'id'=>$id.$i,
				'IsBasicService'=>$hy->IsBasicService,'ItemID'=>$hy->ItemID,'rowOrder'=>($i-1),'ServiceID'=>$hy->ServiceID);
				$h->insert($h_arr);
			}
			$i++;
		}

	}
	public function inserttblHottdeskServiceAvailability(){
		$h_arr=array();
		$id='tblHottdeskServiceAvailability';
		$i=1;
		//To create a table if not exists
		$query="CREATE TABLE IF NOT EXISTS tblHottdeskServiceAvailability (_ID INT NOT NULL PRIMARY KEY AUTO_INCREMENT,_IDNewDataSet char(1) default '1',
				CountryCode char(2),id char(32),rowOrder char(2),ServiceID char(2))";
		$h=new Model_HottdeskServiceAvailability();
		$h->getAdapter()->query($query);
		$h->getAdapter()->query("delete from tblHottdeskServiceAvailability");
		if (count($this->dataSet->tblHottdeskServiceAvailability)!=0)
		foreach ($this->dataSet->tblHottdeskServiceAvailability as $hy){
			$row=$h->select()->where('rowOrder=?',($i-1));
			$test=$h->fetchAll($row)->toArray();
			if(empty($test)){
				$h_arr=array('CountryCode'=>$hy->CountryCode,'id'=>$id.$i,
				'rowOrder'=>($i-1),'ServiceID'=>$hy->ServiceID);
				$h->insert($h_arr);
			}
			$i++;
		}

	}
	public function inserttblLanguage(){
		$h_arr=array();
		$id='tblLanguage';
		$i=1;
		//To create a table if not exists
		$query="CREATE TABLE IF NOT EXISTS tblLanguage(_ID INT NOT NULL PRIMARY KEY AUTO_INCREMENT,_IDNewDataSet char(1) default '1',id char(12),
       	            LanguageCode char(3),LanguageID char(1),LanguageName char(19),NameDescending char(5),rowOrder char(1))";
		$h = new Model_Language();
		$h->getAdapter()->query($query);
		$h->getAdapter()->query("delete from  tblLanguage");
		if (count($this->dataSet->tblLanguage)!=0)
		foreach ($this->dataSet->tblLanguage as $hy){
			$row=$h->select()->where('rowOrder=?',($i-1));
			$test=$h->fetchAll($row)->toArray();
			if(empty($test)){
				$h_arr=array('id'=>$id.$i,'LanguageCode'=>$hy->LanguageCode,'LanguageID'=>$hy->LanguageID,'LanguageName'=>$hy->LanguageName,
				'NameDescending'=>$hy->NameDescending,'rowOrder'=>($i-1));
				$h->insert($h_arr);
			}
			$i++;
		}

	}
	public function inserttblLocationPhoneInfo(){
		$h_arr=array();
		$id='tblLocationPhoneInfo';
		$i=1;
		//To create a table if not exists
		$query="CREATE TABLE IF NOT EXISTS tblLocationPhoneInfo (_ID INT NOT NULL PRIMARY KEY AUTO_INCREMENT,_IDNewDataSet char(1) default '1',
               AreaPrefix char(3),CountryPrefix char(4),ExtensionPrefix char(4),id char(22),LocationCode char(3),rowOrder char(2))";
		$h=new Model_LocationPhoneInfo();
		$h->getAdapter()->query($query);
		$h->getAdapter()->query("delete from tblLocationPhoneInfo");
		if (count($this->dataSet->tblLocationPhoneInfo)!=0)
		foreach ($this->dataSet->tblLocationPhoneInfo as $hy){
			$row=$h->select()->where('rowOrder=?',($i-1));
			$test=$h->fetchAll($row)->toArray();
			if(empty($test)){
				$h_arr=array('AreaPrefix'=>$hy->AreaPrefix,'CountryPrefix'=>$hy->CountryPrefix,'ExtensionPrefix'=>$hy->ExtensionPrefix,'id'=>$id.$i,
				'LocationCode'=>$hy->LocationCode,'rowOrder'=>($i-1));
				$h->insert($h_arr);
			}
			$i++;
		}

	}
	public function inserttblOfficePortal(){
		$h_arr=array();
		$id='tblOfficePortal';
		$i=1;
		//To create a table if not exists
		$query="CREATE TABLE IF NOT EXISTS tblOfficePortal (_ID INT NOT NULL PRIMARY KEY AUTO_INCREMENT,_IDNewDataSet char(1) default '1',
				Description char(16),FTPURL char(27),id char(16),OfficeURL char(22),PortalCode char(1),rowOrder char(1))";
		$h=new Model_OfficePortal();
		$h->getAdapter()->query($query);
		$h->getAdapter()->query("delete from tblOfficePortal");
		if (count($this->dataSet->tblOfficePortal)!=0)
		foreach ($this->dataSet->tblOfficePortal as $hy){
			$row=$h->select()->where('rowOrder=?',($i-1));
			$test=$h->fetchAll($row)->toArray();
			if(empty($test)){
				$h_arr=array('Description'=>$hy->Description,'FTPURL'=>$hy->FTPURL,'id'=>$id.$i,'OfficeURL'=>$hy->OfficeURL,
				'PortalCode'=>$hy->PortalCode,'rowOrder'=>($i-1));
				$h->insert($h_arr);
			}
			$i++;
		}

	}
	public function inserttblTaskTimerGroup(){
		$h_arr=array();
		$id='tblTaskTimerGroup';
		$i=1;
		//To create a table if not exists
		$query="CREATE TABLE IF NOT EXISTS tblTaskTimerGroup (_ID INT NOT NULL PRIMARY KEY AUTO_INCREMENT,_IDNewDataSet char(1) default '1',
				Description char(31),GroupID char(1),id char(18),rowOrder char(1),Title char(4))";
		$h=new Model_TaskTimerGroup();
		$h->getAdapter()->query($query);
		$h->getAdapter()->query("delete from tblTaskTimerGroup");
		if(count($this->dataSet->tblTaskTimerGroup)!=0)
		foreach ($this->dataSet->tblTaskTimerGroup as $hy){
			$row=$h->select()->where('rowOrder=?',($i-1));
			$test=$h->fetchAll($row)->toArray();
			if(empty($test)){
				$h_arr=array('Description'=>$hy->Description,'GroupID'=>$hy->GroupID,'id'=>$id.$i,
				'rowOrder'=>($i-1),'Title'=>$hy->Title);
				$h->insert($h_arr);
			}
			$i++;
		}

	}
	public function insertview_HottdeskService(){
		$h_arr=array();
		$id='view_HottdeskService';
		$i=1;
		//To create a table if not exists
		$query="CREATE TABLE IF NOT EXISTS view_HottdeskService (_ID INT NOT NULL PRIMARY KEY AUTO_INCREMENT,_IDNewDataSet char(1) default '1',
				CurrencyCode char(3),Description char(26),id char(23),ItemID char(8),Price char(11),
				rowOrder char(3),ServiceID char(2),SiteID char(5))";
		$h=new Model_viewHottdeskService();
		$h->getAdapter()->query($query);
		$h->getAdapter()->query("delete from view_HottdeskService");
		if(count($this->dataSet->view_HottdeskService)!=0)
		foreach ($this->dataSet->view_HottdeskService as $hy){
			$row=$h->select()->where('rowOrder=?',($i-1));
			$test=$h->fetchAll($row)->toArray();
			if(empty($test)){
				$h_arr=array('CurrencyCode'=>$hy->CurrencyCode,'Description'=>$hy->Description,'id'=>$id.$i,'ItemID'=>$hy->ItemID,
				'Price'=>$hy->Price,'rowOrder'=>($i-1),'ServiceID'=>$hy->ServiceID,'SiteID'=>$hy->SiteID);
				$h->insert($h_arr);
			}
			$i++;
		}

}
}