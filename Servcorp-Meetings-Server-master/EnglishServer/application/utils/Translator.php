<?php

class Translator {
	
	var $lang;
	var $lookup;
	
	public function __construct() {
		# work out language
		
		if (substr($_SERVER['HTTP_HOST'], -2) == 'jp') {
				$this->lang = 'ja';
				date_default_timezone_set('Asia/Tokyo');
		} else {
				$this->lang = 'en';			
				date_default_timezone_set('Australia/Sydney');
		}
		
//		if ($_SERVER['REMOTE_ADDR'] == '59.167.171.177') {
//			$this->lang = 'ja';
//			date_default_timezone_set('Asia/Tokyo');		
//		}
		
		# load language file
		if ($this->lang != 'en') {
			include_once(APPLICATION_PATH . "/languages/$this->lang.php");
			$this->lookup = $lookup;
		}
	}

	public function translate($string) {
		if ($this->lang == 'en') return $string;
		
		else {
			if (array_key_exists($string, $this->lookup)) {
				return $this->lookup[$string];
			}
			else {
				//die("$string does not exist");
				return '';
			}
		}
	}
}