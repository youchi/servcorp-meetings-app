<?php
/**
 * Current Head meta doesn't support property meta tag so adding that ;) 
 */
class NNT_Misc_View_Helper_HeadMeta extends Zend_View_Helper_HeadMeta
{
	protected $_typeKeys     = array('name', 'http-equiv', 'charset', 'property');
}