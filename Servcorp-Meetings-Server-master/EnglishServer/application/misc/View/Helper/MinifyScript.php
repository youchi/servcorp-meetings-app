<?php

/**
 * Just an extension to the headScript helper that will use
 * Inspiration: http://code.google.com/p/minify/issues/detail?id=105
 * @author vinu
 *
 */

class Misc_View_Helper_MinifyScript extends Zend_View_Helper_HeadScript 
{
	private  $MAX_FILES = 5;
	
	/**
	 * Call this in the layout
	 */
	
	public function minifyScript()
	{
		$orignalHeadScript = $this->view->getHelper("headScript");
		
		$sources = array();
		$removeNodes = array();
		
		$section = 0;
		$index = 0;

		foreach($orignalHeadScript as $item)
		{
			$src = "";
			
			if(isset($item->attributes["src"]) && (strpos($item->attributes["src"],"http") === false))
			{
				$src = $item->attributes["src"];
				$sources[$section][] = $src;
				$removeNodes[$section][] = $index;
			}
			else
			{
				$section ++;
			}
			$index ++;						
		}
		

		for($i=0; $i<=$section; $i++)
		{
			if(isset($removeNodes[$i]))
			{
				$minifyItems = array();
				$nodesToRemove = $removeNodes[$i];
				$sourcesToMin = $sources[$i];
				
				$countSourceFiles = count($sourcesToMin);
				
				if($countSourceFiles >  $this->MAX_FILES)
				{
					$numParts = (int) ($countSourceFiles / $this->MAX_FILES);
										
					$remaining = false;
					
					if($countSourceFiles % $this->MAX_FILES != 0 )
						$remaining = true;

					
					$offSet = 0;

					while($numParts)
					{
						$sourcesToMinSlice = array_slice($sourcesToMin,$offSet,$this->MAX_FILES);
						$filesAsString = implode("," ,$sourcesToMinSlice);
						$minifyItems[] = $this->createMinifyItem($filesAsString);
						
						$offSet += $this->MAX_FILES;
						$numParts --;
					}
					
					if($remaining)
					{
						$sourcesToMinSlice = array_slice($sourcesToMin,$offSet);
						$filesAsString = implode("," ,$sourcesToMinSlice);
						$minifyItems[] = $this->createMinifyItem($filesAsString);
					}
										
				}				
				else 
				{
					$filesAsString = implode("," ,$sourcesToMin);
					$minifyItems[] = $this->createMinifyItem($filesAsString);
				}
				
				foreach($nodesToRemove as $nodeToRemove)
				{
					unset($orignalHeadScript[$nodeToRemove]);
				}
				
				$index = 0;
				foreach($minifyItems as $minifyItem)
				{
					$orignalHeadScript[$nodesToRemove[$index]] = $minifyItem;	
					$index ++;
				}					
			}			
		}
		
		return $orignalHeadScript;
	}

	
	private function createMinifyItem($src)
	{
		$attr = array();
		$attr["src"] = "min/index.php?f=$src";
		$item = $this->createData("text/javascript",$attr);		
		return $item;
	}
}