<?php
/*       INSTRUCTIONS     

Enter the translation where indicated between the second set of quotes for each phrase in the below array.  e.g.:

"Meetings" => "ミーティング",

*/

$lookup = array(

/* URLs  */
"http://www.servcorponline.com.au/Login.aspx?Mode=R" => "http://www.servcorponline.co.jp/Login.aspx?Mode=R",

/*  General  */

"loading" => "読み込み中です",
"Back" => "戻る",
"Cancel" => "キャンセル",
"Logout" => "ログアウト",

/*  Login Page  */

"Meetings Login" => "会議室予約 ログイン",
"User ID" => "ユーザーID",
"Password" => "パスワード",
"Remember me" => "ログイン情報を保存する",
"Invalid username or password. Please try again." => "ユーザー名もしくはパスワードが異なります。再度お試し下さい。",
"Login" => "ログイン",
"Forgot your login details?" => "ユーザー名、パスワードを忘れた方はこちら",
"If you don't have a Meetings Online account please tap the button below to register now" => "会議室予約のオンラインアカウントをお持ちでない方は、下記の登録ボタンをタップしてください。",
"Register" => "登録する",

/*  Dashboard  */

"Meetings" => "ミーティング",
"My Bookings" => "お客様のご予約",
"New Booking" => "新規ご予約",
"Refreshing bookings..." => "ご予約を再読み込みしています",
"Currently you have no future bookings." => "現在承っているご予約はありません",

/*  New Bookings  */
"Select Country" => "国をお選び下さい",
"Select Location" => "拠点をお選び下さい",
"Select City" => "都市をお選び下さい",
"Select Resource Type" => "サービスをお選び下さい",
"Select Resource" => "サービスをお選び下さい",
"Select Date" => "日付をお選び下さい",
"Select Time" => "時間帯をお選び下さい",
"Start Time" => "開始時間",
"Duration" => "ご利用時間",
"Done" => "完了",
'For bookings on weekends please <a href="javascript:void(0);" id="email1" rel="external">email us</a> or call us: <a href="javascript:void(0);" id="call1">call us</a>' => '週末のご予約は、サーブコープまで<a href="javascript:void(0);" id="email1" rel="external">メールにてご連絡下さい</a> お電話にてご連絡下さい<a href="javascript:void(0);" id="call1">お電話にてご連絡下さい</a>',
'For bookings after hours please <a href="javascript:void(0);" id="email1" rel="external">email us</a> or call us: <a href="javascript:void(0);" id="call1">call us</a>'  =>'営業時間外のご予約は、サーブコープまで<a href="javascript:void(0);" id="email1" rel="external">メールにてご連絡下さい</a> お電話にてご連絡下さい<a href="javascript:void(0);" id="call1">お電話にてご連絡下さい</a>',
'+65 6322 0888'  => '+65 6322 0888',
"Extras" => "その他のご要望",
"Enter any extra requirements (or tap Continue)" => "その他のご要望がございましたらご入力下さい。（または「続ける」をタップして下さい）",
"Continue" => "続ける",
"Send Invites" => "招待Eメールを送信",
"Separate emails with semicolons (or tap Continue)" => "招待Eメールを送信する場合は、参加者のEメールアドレスを入力して下さい。2つ以上入力する場合は、セミコロンで区切って下さい。（または「続ける」をタップして下さい）",
"This resource has been fully booked for the time selected. Please choose a different time or resource." => "申し訳ありませんが、ご希望のサービス、時間帯は既にご予約済みでご利用いただけません。他の時間帯、またはサービス内容をお選び下さい。",
"Please contact Servcorp Staff for booking on weekends / public holidays." => "週末および祝・休日のご予約は、サーブコープチームまでご連絡下さい",
"The time you selected for this resource is outside its available hours. Please contact a Servcorp team member to make this booking." => "ご希望の時間帯は、このサービスのご利用可能時間外となっております。他の時間帯をお選び下さい。",
"Thank you for making a booking. Your booking has been confirmed." => "ご予約ありがとうございます。詳細を承りました。",
"Please fill in the details." => "詳細を記入して下さい",

/*  Edit Bookings  */

"Details" => "詳細",
"Edit Booking" => "ご予約の変更",
"Delete" => "削除",
"Phone" => "電話",
"Fax" => "ファックス",


/*  Alerts  */
"Please enter your username and password." => "ユーザー名とパスワードをご入力下さい",
"Please enter your username." => "ユーザー名をご入力下さい",
"Please enter your password." => "パスワードをご入力下さい",
"Invalid username or password." => "ユーザー名もしくはパスワードが異なります",
"Refreshing country list..." => "国名を再読込みしています…",
"Refreshing location list..." => "サービス拠点を再読込みしています…",
"Refreshing resource types..." => "サービス内容を再読込みしています…",
"Refreshing resources list..." => "サービス内容を再読込みしています…",
"Tap here to open in Google Maps" => "グーグルマップを開く場合は、こちらをタップして下さい",
"Sorry, bookings for today cannot be edited." => "当日のご予約は変更できません。",
"Please fill in the details." => "詳細を記入して下さい",
"Are you sure you wish to delete this booking?" => "この予約をキャンセルしてよろしいですか？",
"This resource has been fully booked for the time selected. Please choose a different time or resource." => "申し訳ありませんが、ご希望のサービス、時間帯は既にご予約済みでご利用いただけません。他の時間帯、またはサービス内容をお選び下さい。",
"Please contact Servcorp Staff for booking on weekends / public holidays." => "週末および祝・休日のご予約は、サーブコープチームまでご連絡下さい。",
"The time you selected for this resource is outside its available hours. Please contact a Servcorp team member to make this booking." => "ご希望の時間帯は、このサービスのご利用可能時間外となっております。他の時間帯をお選び下さい。",
"Your booking has been updated." => "ご予約内容が変更されました",
"Booking deleted" => "ご予約がキャンセルされました",
"Delete booking failed, please try again" => "ご予約がキャンセルできませんでした。再度お試し下さい。",
"JAN" => "1月",
"FEB" => "2月",
"MAR" => "3月",
"APR" => "4月",
"MAY" => "5月",
"JUN" => "6月",
"JUL" => "7月",
"AUG" => "8月",
"SEP" => "9月",
"OCT" => "10月",
"NOV" => "11月",
"DEC" => "12月",
"You can only edit date and time" => "日付と時間帯のみ編集可能です",
"Select a start time" => "開始時間をお選び下さい",
"Select a duration" => "ご利用時間をお選び下さい",
"Please contact the Servcorp Staff for after hour bookings." => "営業時間外のご予約は、サーブコープチームまでご連絡下さい。",
"Email us" => "Eメールでのお問合せ",
"Call us" => "電話でのお問合せ",
"Select a time slot" => "時間帯をお選び下さい",
"Oops! We were unable to connect to the server. Please try again." => "サーバーに接続できませんでした。再度お試し下さい。",
"No resources available for this location" => "このサービス拠点ではご利用可能なサービスがありません",
"No resources available" => "ご利用可能なサービスがありません",

);

?>