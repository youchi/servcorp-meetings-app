<?php

//Execute this script using php DBUpdateScript.php -e <env>

defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../'));

set_include_path(implode(PATH_SEPARATOR, array(
    APPLICATION_PATH . '/../library',
    get_include_path(),
)));

require_once 'Zend/Loader/Autoloader.php';
Zend_Loader_Autoloader::getInstance();

$getopt = new Zend_Console_Getopt(array(
    'env|e-s'    => 'Application environment for which to send mails (defaults to development)',
    'help|h'     => 'Help -- usage message',
	));

try
{
    $getopt->parse();
} catch (Zend_Console_Getopt_Exception $e) {
    // Bad options passed: report usage
    echo $e->getUsageMessage();
    return false;
}

function printmsg($msg)
{
	echo $msg;
	echo  PHP_EOL;
}

$env   = $getopt->getOption('e');


$possibleEnvs = array('staging',"production","development","testing");

if(!in_array($env,$possibleEnvs))
{
	printmsg("Invalid env  specified. Possible values are " . print_r($possibleEnvs,true));
	return false;
}

defined('APPLICATION_ENV') || define('APPLICATION_ENV', (null === $env) ? 'development' : $env);

require_once 'Zend/Application.php';
$application = new Zend_Application(APPLICATION_ENV,APPLICATION_PATH . '/configs/application.ini');
$bootstrap = $application->getBootstrap();
$bootstrap->bootstrap('db');
$bootstrap->getResourceLoader()->addResourceType('servcorp_utils','utils/','UTILS');

//Logger
/*$stream = fopen('../../public/log/debug_log', 'a', false);
$writer = new Zend_Log_Writer_Stream($stream);
$logger = new Zend_Log($writer);
Zend_Registry::set('logger', $logger);*/

//soap credentials
$config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', APPLICATION_ENV);
Zend_Registry::set('soapOptions', array("login"=>$config->soap->login,"password"=>$config->soap->password));

//updating the db
$bookingXml=new UTILS_BookingDataParser();
$bookingXml->insertCountry();
echo "Country ";
$bookingXml->insertCity();
echo  "City ";
$bookingXml->insertLocation();
echo "Location ";
$bookingXml->insertResource();
echo "Resource ";
$bookingXml->insertResourceType();
echo "ResourceType ";
$bookingXml->insertSite();
echo "Site ";
$bookingXml->insertHoliday();
echo "Holiday ";
$bookingXml->insertWeekly();
echo "Weekly ";
$bookingXml->inserttblHottdeskService();
echo "HottdeskService ";
$bookingXml->inserttblHottdeskServiceAvailability();
echo "HottdeskServiceAvailability ";
$bookingXml->inserttblLanguage();
echo "Language ";
$bookingXml->inserttblLocationPhoneInfo();
echo "LocationPhoneInfo ";
$bookingXml->inserttblOfficePortal();
echo "OfficePortal ";
$bookingXml->inserttblTaskTimerGroup();
echo "TaskTimerGroup ";
$bookingXml->insertview_HottdeskService();

echo "DB updated successfully";

