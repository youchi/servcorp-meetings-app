<?php
class Model_Site extends Zend_Db_Table_Abstract {

	protected $_name="tblSite";
	public function getFaxPhoneLoc($SiteID) {
		$select = $this->select()->setIntegrityCheck(false);
		$select->from($this,array('Fax','Phone','LocationCode'))
		->where('SiteID=?',$SiteID);
		return $this->fetchAll($select);
	}
	public function getEmailPhone($SiteID){
		$select = $this->select()->setIntegrityCheck(false);
		$select->from($this,array('Email','Phone'))
		->where('SiteID=?',$SiteID);
		return $this->fetchAll($select);
	}
}