<?php
class Model_Location extends Zend_Db_Table_Abstract {
	
	protected $_name="tblLocation";
	public function getLocationFromCountry($country) {
		/*select * from  tblLocation lo inner join tblCity ci on lo.CityCode=ci.CityCode where ci.CountryCode='HK'*/
		
		$select = $this->select()->setIntegrityCheck(false);
		$select->from(array('lo'=>'tblLocation'),array('Description','CityCode','LocationCode'))
			   ->join(array('ci'=>'tblCity'), 'lo.CityCode=ci.CityCode', array())
			   ->where('ci.CountryCode=?',$country)
			   ->order('lo.Description');
		return $this->fetchAll($select);
	}
}