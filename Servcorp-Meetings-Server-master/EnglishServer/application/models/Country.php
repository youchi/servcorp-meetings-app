<?php
class Model_Country extends Zend_Db_Table_Abstract {

	protected $_name="tblCountry";
	public function getCountry() {
		$select = $this->select()->setIntegrityCheck(false);
		$select->from($this,array('CountryCode','Description'))
		->order(array('Description'));
		$res = $this->fetchAll($select);
		if ($res) return $res;
		else return false;
	}

	public function getCountryFromLoc($locationcode) {
		/*
		 * SELECT t.Description FROM tblCountry t
		 inner join tblCity c on c.CountryCode=t.CountryCode
		 inner join tblLocation l on c.CityCode=l.CityCode
		 inner join tblSite s on l.LocationCode=s.LocationCode where s.SiteID='BAT02';

		 ->from(array('rt'=>'tblResourceType'),array('Description','ResourceTypeID'))
		 ->join(array('r'=>'tblResource'), 'rt.ResourceTypeID=r.ResourceTypeID', array())
		 ->join(array('s'=>'tblSite'), 'r.SiteID=s.SiteID', array())
		 ->where('s.LocationCode=?',$locationcode);

		 */
		$select = $this->select()->setIntegrityCheck(false);
		$select->from(array('t'=>'tblCountry'),array('CountryName'=>'Description'))
		->join(array('c'=>'tblCity'), 'c.CountryCode=t.CountryCode', array())
		->join(array('l'=>'tblLocation'), 'c.CityCode=l.CityCode', array('LocationName'=>'Description','Latitude'=>'HorizontalPosition','Longitude'=>'VerticalPosition'))
		->where('l.LocationCode=?',$locationcode);
		return $this->fetchAll($select)->toArray();
	}
	public function getAllCountryCode(){
		$select = $this->select();
		$select->setIntegrityCheck(false)
		->from($this,'CountryCode');
		$rows = $this->fetchAll($select)->toArray();
		if($rows){
			foreach ($rows as $key)
			$arr[]=$key['CountryCode'];
			return $arr;
		}
		else
		return false;
	}
	public function  getLocation(){
		$select = $this->select()->setIntegrityCheck(false);
		$select->from(array('t'=>'tblCountry'),array('CountryName'=>'Description'))
		->join(array('c'=>'tblCity'), 'c.CountryCode=t.CountryCode', array())
		->join(array('l'=>'tblLocation'), 'c.CityCode=l.CityCode', array('LocationName'=>'Description'))
		->order(array('t.Description'));
		return $this->fetchAll($select)->toArray();
		
	}
}