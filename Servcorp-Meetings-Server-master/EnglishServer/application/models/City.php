<?php
class Model_City extends Zend_Db_Table_Abstract {
	
	protected $_name="tblCity";
	public function getCity($country="") {
		$select = $this->select()->setIntegrityCheck(false);
		if($country)
			$select->from($this,array('CountryCode','CityCode','Description'))->where("CountryCode=?",$country);
		else
			$select->from($this,array('CountryCode','CityCode','Description'));
		return $this->fetchAll($select);
	}
}