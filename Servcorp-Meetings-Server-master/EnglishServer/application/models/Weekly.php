<?php
class Model_Weekly extends Zend_Db_Table_Abstract {
	protected $_name='tblBookingWeeklyRule';
	public function getWorkhourfromSid($dayofweek,$sid){
		/*SELECT OpenHour,OpenMinute,AfterHour,AfterMinute,CloseHour,CloseMinute FROM servcorp_test2.tblBookingWeeklyRule 
		 * where SiteID='ADE01' AND DayOfWeek=1*/
		$select=$this->select()->setIntegrityCheck(false);
		$select->from(array('w'=>'tblBookingWeeklyRule'),array('CloseAllDay'=>'CloseAllDay','OpenHour'=>'OpenHour','OpenMinute'=>'OpenMinute','CloseHour'=>'CloseHour','CloseMinute'=>'CloseMinute'))
		->where('SiteID=?',$sid)
		->where('DayOfWeek=?',$dayofweek);
		return $this->fetchAll($select);
	}
}