<?php
class Model_Resource extends Zend_Db_Table_Abstract {
	
	protected $_name='tblResource';
	public function getRNameFromLoc($RTid,$locationcode) {
		/*
		 * select r.ResourceID,r.ResourceName from tblResource r inner join tblSite s on s.SiteID=r.SiteID where s.LocationCode='PHM' and r.ResourceTypeID=4
		 * */
		$select = $this->select();
		$select->from(array('r'=>'tblResource'), array('ResourceName','ResourceID'))
			   ->join(array('s'=>'tblSite'), 's.SiteID=r.SiteID', array())
			   ->where('s.LocationCode=?',$locationcode)
			   ->where('r.ResourceTypeID=?',$RTid)
			   ->where('r.Status=?',1)
			   ->order('r.ResourceName');
		return $this->fetchAll($select);
	}
	
	public function getResourceDetails($rid) {
		/* SELECT r.ResourceName as ResourceName, rt.Description as ResourceType
		 FROM servcorp.tblResource r inner join tblResourceType rt on r.ResourceTypeID=rt.ResourceTypeID where r.ResourceID=494 */
		$select = $this->select()->setIntegrityCheck(false);
		$select->from(array('r'=>'tblResource'),array('ResourceName'=>'ResourceName'))
			   ->join(array('rt'=>'tblResourceType'),'r.ResourceTypeID=rt.ResourceTypeID',array('ResourceType'=>'Description'))
			   ->where('r.ResourceID=?',$rid);
		return $this->fetchAll($select);
	}
	public function getSidFromRid($rid){
		/*SELECT SiteID FROM servcorp_test2.tblResource where ResourceID='60'*/
		$select=$this->select()->setIntegrityCheck(false);
		$select->from(array('r'=>'tblResource'),array('SiteID'=>'SiteID'))
		      ->where('r.ResourceID=?',$rid);
		      return $this->fetchAll($select);
	}
}