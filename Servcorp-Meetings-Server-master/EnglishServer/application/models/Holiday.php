<?php
class Model_Holiday extends Zend_Db_Table_Abstract {
	protected $_name="tblBookingHolidayRule";
	public function getHolidayFromSid($sid,$date){
		/*SELECT CloseDay,CloseMonth,Closeyear,CloseHour,CloseMinute FROM servcorp_test2.tblBookingHolidayRule where SiteID='ZUE01'*/
		$select=$this->select()->setIntegrityCheck(false);
		$select->from(array('h'=>'tblBookingHolidayRule'),array('CloseDay'=>'CloseDay','CloseMonth'=>'CloseMonth'))
				->where('CloseYear=?',(int)$date[0])
				->where('CloseMonth=?',(int)$date[1])
				->where('CloseDay=?',(int)$date[2])
				->where('SiteID=?',$sid);
		return $this->fetchAll($select);
		
	}
}