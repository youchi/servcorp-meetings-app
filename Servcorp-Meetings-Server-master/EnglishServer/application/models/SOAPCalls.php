<?php

class Model_SOAPCalls{
	protected $soap_obj;
	protected $xml_head;
	var $lang;
	
	public function __construct(){
		//array("login"=>$config->soap->login,"password"=>$config->soap->password)
		//Zend_Registry::get('soapOptions')
		$this->soap_obj = new Zend_Soap_Client("http://mobile.splatform.net:102/MobileWebService.asmx?wsdl",Zend_Registry::get('soapOptions'));
		$this->xml_head = '<?xml version="1.0"?>';
		
		if (substr($_SERVER['HTTP_HOST'], -2) == 'jp') {
				$this->lang = 'JPN';
		} else {
				$this->lang = 'ENG';			
		}
	}

	public function checkLogin($username,$password) {
		//TODO should remove exceptions from here after confirming that login response would be a boolean
		try {
			return $this->soap_obj->Login(array('username'=>$username,'password'=>$password))->LoginResult;
		} catch (Exception $e) {
			return false;
		}
	}

	public function createBooking($booking_param) {
		$ret = $this->soap_obj->CreateBooking($booking_param)->CreateBookingResult->any;
		$xm = new Zend_Config_Xml($this->xml_head.$ret);
		return $xm->BookingDataSet->tblBooking->RefNo;
	}

	public function getDashboardItems($username)
	{
		$ret = $this->soap_obj->GetMyBookingList(array('Username'=>$username,'IncludePast'=>false))->GetMyBookingListResult->any;
		$xm = new Zend_Config_Xml($this->xml_head.$ret);
		$item_arr=array();
		if(count($xm)==0)
		return $item_arr;
		$i = 0;
		$bookings=$xm->NewDataSet->view_BookingSummary->toArray();
		if (sizeof($bookings)!=0)
		if (!array_key_exists(0,$bookings))
		$bookings=array(0=>$bookings);
		foreach ($bookings as $item) {
			//country, location, date, starttime-endtime,phone,fax,
			$sit = new Model_Site();
			$det = $sit->getFaxPhoneLoc($item['SiteID'])->toArray();
			if (!sizeof($det))
			continue;
			$co = new Model_Country();
			$loc=$co->getCountryFromLoc($det[0]['LocationCode']);
			$re = new Model_Resource();
			$resource = $re->getResourceDetails($item['ResourceID'])->toArray();
			$item_arr[$i]['resourceid']=$item['ResourceID'];
			$item_arr[$i]['refno']=$item['RefNo'];
			$item_arr[$i]['fax']=$det[0]['Fax'];
			$item_arr[$i]['phone']=$det[0]['Phone'];
			$item_arr[$i]['date']=date($this->date_str(),strtotime($item['bookingDate']));
			$item_arr[$i]['stime']=$item['StartTime'];
			$item_arr[$i]['etime']=$item['EndTime'];
			$item_arr[$i]['location']=$loc[0]['LocationName'];
			$item_arr[$i]['country']=$loc[0]['CountryName'];
			$item_arr[$i]['resource']=$resource[0]['ResourceName'];
			$item_arr[$i]['resourcetype']=$resource[0]['ResourceType'];
			$item_arr[$i]['comments']=$item['Comments'];
			$item_arr[$i]['latitude']=$loc[0]['Latitude'];
			$item_arr[$i]['longitude']=$loc[0]['Longitude'];
			$i++;
		}
		return array_reverse($item_arr);
	}
	
	private function date_str() {
		if ($this->lang == 'JPN') {
			return 'Y. m. d.';
		} else {
			return 'd F Y';
		}

	}

	public function doUpdateBooking($update_data)
	{
		$ret = $this->soap_obj->UpdateBooking($update_data);
		return $ret;
	}
	
	public function doCancelBooking($uname,$refno)
	{
		return $this->soap_obj->CancelBooking(array('RefNo'=>$refno,'username'=>$uname));
	}
	public function  getResourceBooked($username,$includepast)
	{
		return $this->soap_obj->GetMyBookingList(array('Username'=>$username,'IncludePast'=>$includepast));
	}
	public function doBookingMail($refno){
		return $this->soap_obj->SendEmailBookingInvitationToClient(array('RefNo'=>$refno,'LanguageCode'=>$this->lang))->SendEmailBookingInvitationToClientResult;
	}
        public function doBookingInviteeMail($refno){
		return $this->soap_obj->SendEmailBookingInvitationToInvitee(array('RefNo'=>$refno,'LanguageCode'=>$this->lang))->SendEmailBookingInvitationToInviteeResult;
	}
        public function doBookingFloorMail($refno){
		return $this->soap_obj->SendEmailBookingInvitationToFloor(array('RefNo'=>$refno,'LanguageCode'=>$this->lang))->SendEmailBookingInvitationToFloorResult;
	}
	public function doUpdateBookingMail($refno){
		return $this->soap_obj->SendEmailBookingUpdateToClient(array('RefNo'=>$refno,'LanguageCode'=>$this->lang))->SendEmailBookingUpdateToClientResult;
	}
        public function doUpdateInviteeMail($refno){
		return $this->soap_obj->SendEmailBookingUpdateToInvitee(array('RefNo'=>$refno,'LanguageCode'=>$this->lang))->SendEmailBookingUpdateToInviteeResult;
	}
        public function doUpdateFloorMail($refno){
		return $this->soap_obj->SendEmailBookingInvitationToFloor(array('RefNo'=>$refno,'LanguageCode'=>$this->lang))->SendEmailBookingInvitationToFloorResult;
	}
	public function doCancelBookingMail($refno){
		return $this->soap_obj->SendEmailBookingCancellationToClient(array('RefNo'=>$refno,'LanguageCode'=>$this->lang))->SendEmailBookingCancellationToClientResult;
	}
	public function doCancelInviteeMail($refno){
		return $this->soap_obj->SendEmailBookingCancellationToInvitee(array('RefNo'=>$refno,'LanguageCode'=>$this->lang))->SendEmailBookingCancellationToInviteeResult;
	}
        public function doCancelFloorMail($refno){
		return $this->soap_obj->SendEmailBookingUpdateToFloor(array('RefNo'=>$refno,'LanguageCode'=>$this->lang))->SendEmailBookingUpdateToFloorResult;
	}
	public function getTimeSlots($date,$resourceid) {
		$dat=explode(' ',date('d m Y',strtotime($date)));
		$res=new Model_Resource();
		$s=$res->getSidFromRid($resourceid)->toArray();
		$site=new Model_Site();
		$si=$site->getEmailPhone($s[0]['SiteID'])->toArray();
		$ret=$this->soap_obj->GetResourceBooked(array('ResourceID'=>$resourceid,'Year'=>$dat[2],'Month'=>$dat[1],'Day'=>$dat[0]))->GetResourceBookedResult->any;
		$xm = new Zend_Config_Xml($this->xml_head.$ret);
		if(count($xm)==0){
			return $si;
		}
		$slot=$xm->NewDataSet->tblResourceBookedView->toArray();
		if(sizeof($slot)!=0)
		if(!array_key_exists(0, $slot)){
			$slot=array(0=>$slot);
			if(count($slot)==1)
			return array(array('StartHour'=>$slot[0]['Hour_b'],'StartMin'=>$slot[0]['Minute_b'],
							'EndHour'=>$slot[0]['Hour_b'],'EndMin'=>$slot[0]['Minute_b']+10,'Email'=>$si[0]['Email'],'Phone'=>$si[0]['Phone']));
		}
		$k=0;
		$z=0;
		for($i=$k;$i<count($slot)-1;$i++){
			if($slot[$i+1]['RefNo']-$slot[$i]['RefNo']==0)
			continue;
			else {
				$slot[$i]['Minute_b']=(string)($slot[$i]['Minute_b']+10);
				if($slot[$i]['Minute_b']>=60){
					$slot[$i]['Minute_b']="0";
					$slot[$i]['Hour_b']=(string)($slot[$i]['Hour_b']+1);
				}
				$booking[$z++]=array('StartHour'=>$slot[$k]['Hour_b'],'StartMin'=>$slot[$k]['Minute_b'],
				                        'EndHour'=>$slot[$i]['Hour_b'],'EndMin'=>$slot[$i]['Minute_b'],'Email'=>$si[0]['Email'],'Phone'=>$si[0]['Phone']);
				$k=$i+1;
				continue;
			}
		}
		$slot[$i]['Minute_b']=(string)($slot[$i]['Minute_b']+10);
		if($slot[$i]['Minute_b']>=60){
			$slot[$i]['Minute_b']="0";
			$slot[$i]['Hour_b']=(string)($slot[$i]['Hour_b']+1);
		}
		$booking[$z ]=array('StartHour'=>$slot[$k]['Hour_b'],'StartMin'=>$slot[$k]['Minute_b'],
							'EndHour'=>$slot[$i]['Hour_b'],'EndMin'=>$slot[$i]['Minute_b'],'Email'=>$si[0]['Email'],'Phone'=>$si[0]['Phone']);
		return $booking;
	}
	public function getBookedResource(){
	 return $this->soap_obj->GetResourceDataForBooking()->GetResourceDataForBookingResult->any;
	}
	public function getLocalisedDescriptions(){
	 return $this->soap_obj->GetLocalisedDescrptions(array('LanguageCode'=>$this->lang))->GetLocalisedDescrptionsResult->any;
	}
}

