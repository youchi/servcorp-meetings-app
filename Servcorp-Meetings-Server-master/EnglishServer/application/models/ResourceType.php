<?php
class Model_ResourceType extends Zend_Db_Table_Abstract {
	
	protected $_name='tblResourceType';
	public function getResourceTypes() {
		$select = $this->select()->setIntegrityCheck(false);
		$select->from($this,array('ResourceTypeID','Description'));
		return $this->fetchAll($select);
	}
	public function getResourceTypeFromLocation($locationcode) {
		/*
		 * SELECT SiteID FROM tblSite t where LocationCode='ADE'
		 * SELECT DISTINCT ResourceTypeID FROM tblResource t where SiteID IN ('ADE01','ADE02','ADE03','ADE04')
		 * SELECT * FROM tblResourceType t where ResourceTypeID=1
		 * 
		 * select DISTINCT rt.Description from tblResourceType rt 
		 * inner join tblResource r on rt.ResourceTypeID=r.ResourceTypeID 
		 * inner join tblSite s on r.SiteID=s.SiteID where s.LocationCode='ADE'
		 */
		$select = $this->select()
					   ->distinct()
					   ->from(array('rt'=>'tblResourceType'),array('Description','ResourceTypeID'))
					   ->join(array('r'=>'tblResource'), 'rt.ResourceTypeID=r.ResourceTypeID', array())
					   ->join(array('s'=>'tblSite'), 'r.SiteID=s.SiteID', array())
					   ->where('s.LocationCode=?',$locationcode)
					   ->order('rt.Description');
					   
		return $this->fetchAll($select);
	}
}