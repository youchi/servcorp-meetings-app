<?php
require_once 'SOAPAuth.php';
class LoginController extends BaseController {

	public function indexAction() {
		$log = Zend_Registry::get('logger');
		$credentials = $this->getRequest()->getParams();
		$auth = Zend_Auth::getInstance();
		$authAdapter = new SOAPAuth($credentials['username'],$credentials['password']);
		$result = $auth->authenticate($authAdapter);
		var_dump($result);
		if($result->isValid()) {
			Zend_Session::start();
			if($credentials['rememberme']=="true") 
				Zend_Session::rememberMe(60*60*24*30);
			$this->_helper->json->sendJson(array('login'=>true));
		}
		$this->_helper->json->sendJson(array('login'=>false));
	}
}