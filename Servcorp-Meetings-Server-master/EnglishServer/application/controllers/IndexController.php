<?php

class IndexController extends BaseController
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {   
    	$this->view->loggedin=false;
    	if (Zend_Session::sessionExists() && Zend_Auth::getInstance()->getStorage()->read())
    		$this->view->loggedin=true;
     }
}

