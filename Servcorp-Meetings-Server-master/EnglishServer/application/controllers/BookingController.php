<?php
class BookingController extends BaseController
{
	protected $log;
	protected $uname;
	public function init()
	{
		$this->log = Zend_Registry::get('logger');
		$this->uname = Zend_Auth::getInstance()->getStorage()->read();
	}
	public function indexAction()
	{
		if($this->getRequest()->ispost())
		{
			$det=$this->getRequest()->getParams();
			$booking_param=array('UserName'=>$this->uname,'ResourceID'=>$det['resourceid'],
								 'SelectedDate'=>date('Ymd',strtotime($det['date'])),'StartHour'=>$det['s_hr'],'StartMin'=>$det['s_min'],'EndHour'=>$det['e_hr'],'EndMin'=>$det['e_min'],
								 'Comments'=>$det['comments'],'Invitees'=>$det['invitees']);
			$soapObj  = new Model_SOAPCalls();
			$res=new Model_Resource();
			$s=$res->getSidFromRid($booking_param["ResourceID"])->toArray();
			$hol=new Model_Holiday();
			$item=$hol->getHolidayFromSid($s[0]['SiteID'],explode('-', date('Y-m-d',strtotime($det["date"]))))->toArray();
			if(empty($item)){
				$dayofweek=date('w',strtotime($booking_param['SelectedDate']));
				$week=new Model_Weekly();
				$tm=$week->getWorkhourfromSid($dayofweek,$s[0]['SiteID'])->toArray();
				if (($tm[0]['CloseAllDay'] == "true") || date('Hi',mktime($booking_param['StartHour'],$booking_param['StartMin']))<date('Hi',mktime($tm[0]["OpenHour"],$tm[0]["OpenMinute"]))||
				date('Hi',mktime($booking_param['StartHour'],$booking_param['StartMin']))>date('Hi',mktime($tm[0]["CloseHour"],$tm[0]["CloseMinute"]))||
				date('Hi',mktime($booking_param['EndHour'],$booking_param['EndMin']))<date('Hi',mktime($tm[0]["OpenHour"],$tm[0]["OpenMinute"]))||
				date('Hi',mktime($booking_param['EndHour'],$booking_param['EndMin']))>date('Hi',mktime($tm[0]["CloseHour"],$tm[0]["CloseMinute"])))
				$this->_helper->json->sendJson(array('refno'=>'2'));
				else {
					try{
						$refno=$soapObj->createBooking($booking_param);
						$soapObj->doBookingMail($refno);
						$soapObj->doBookingInviteeMail($refno);
                                                $soapObj->doBookingFloorMail($refno);
					}
					catch(Exception $e){
						$this->_helper->json->sendJson(array('refno'=>'0'));
					}
					$this->_helper->json->sendJson(array('refno'=>$refno));
				}
			}
			else
			$this->_helper->json->sendJson(array('refno'=>'1'));;

		}
		elseif ($this->getRequest()->isget()) {
			$params = $this->getRequest()->getParams();
			if($params['value']=='country') {
				$country = new Model_Country();
				$this->_helper->json->sendJson(array('country'=>$country->getCountry()->toArray()));
			}
			elseif ($params['value']=='city') {
				$location = new Model_Location();
				$this->_helper->json->sendJson($location->getLocationFromCountry($params['country'])->toArray());
			}
			elseif ($params['value']=='resourcetype') {
				$rt = new Model_ResourceType();
				
				$this->_helper->json->sendJson($rt->getResourceTypeFromLocation($params['location'])->toArray());
				// var
				// var_dump($rt->getResourceTypeFromLocation('SBS'));
			}
			elseif ($params['value']=='resource') {
				$resource = new Model_Resource();
				// var_dump($params['RTid']);
				$this->_helper->json->sendJson($resource->getRNameFromLoc($params['RTid'], $params['location'])->toArray());
			}
			elseif ($params['value']=='timeslot') {
				$soapObj  = new Model_SOAPCalls();
				$this->_helper->json->sendJson($soapObj->getTimeSlots($params['date'],$params['resourceid']));
			}
			elseif ($params['value']=='date'){
				$res=new Model_Resource();
				$s=$res->getSidFromRid($params['resourceid'])->toArray();
				$site=new Model_Site();
				$this->_helper->json->sendJson($site->getEmailPhone($s[0]['SiteID'])->toArray());
			}
		}

	}

	public function deleteAction() {
		if($this->getRequest()->ispost()) {
			$params =$this->getRequest()->getParams();
			$soapObj  = new Model_SOAPCalls();
			$res = $soapObj->doCancelBooking($this->uname,$params['refno'])->CancelBookingResult;
			$soapObj->doCancelBookingMail($params['refno']);
			$soapObj->doCancelInviteeMail($params['refno']);
                        $soapObj->doCancelFloorMail($params['refno']);
			$this->_helper->json->sendJson(array('result'=>$res));
		}
	}

	public function editAction() {
		if($this->getRequest()->ispost()) {
			$par = $this->getRequest()->getParams();
			$soapObj  = new Model_SOAPCalls();
			$data = array('RefNo'=>$par['refno'],'EditDate'=>date('Ymd',strtotime($par['date'])),'StartHour'=>$par['s_hr'],'StartMin'=>$par['s_min'],
						'EndHour'=>$par['e_hr'],'EndMin'=>$par['e_min'],'Comments'=>$par['comments'],'Invitees'=>$par['invitees']);
			$res=new Model_Resource();
			$s=$res->getSidFromRid($par['resourceid'])->toArray();
			$hol=new Model_Holiday();
			$item=$hol->getHolidayFromSid($s[0]['SiteID'],explode('-', date('Y-m-d',strtotime($par["date"]))))->toArray();
			if(empty($item)){
				$dayofweek=date('w',strtotime($data['EditDate']));
				$week=new Model_Weekly();
				$tm=$week->getWorkhourfromSid($dayofweek,$s[0]['SiteID'])->toArray();
				if (date('Hi',mktime($data['StartHour'],$data['StartMin']))<date('Hi',mktime($tm[0]["OpenHour"],$tm[0]["OpenMinute"]))||
				date('Hi',mktime($data['StartHour'],$data['StartMin']))>date('Hi',mktime($tm[0]["CloseHour"],$tm[0]["CloseMinute"]))||
				date('Hi',mktime($data['EndHour'],$data['EndMin']))<date('Hi',mktime($tm[0]["OpenHour"],$tm[0]["OpenMinute"]))||
				date('Hi',mktime($data['EndHour'],$data['EndMin']))>date('Hi',mktime($tm[0]["CloseHour"],$tm[0]["CloseMinute"])))
				$this->_helper->json->sendJson(array('result'=>'2'));
				else {
					try{
						$refno=$soapObj->doUpdateBooking($data);
						$soapObj->doUpdateBookingMail($data['RefNo']);
						$soapObj->doUpdateInviteeMail($data['RefNo']);
                                                $soapObj->doUpdateFloorMail($data['RefNo']);
					}
					catch(Exception $e){
						$this->_helper->json->sendJson(array('result'=>'0'));
					}
					$this->_helper->json->sendJson(array('result'=>'3'));
				}
			}
			else
			$this->_helper->json->sendJson(array('result'=>'1'));
		}
	}

}