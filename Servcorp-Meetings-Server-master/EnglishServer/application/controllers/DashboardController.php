<?php
//require_once '../application/models/SOAPCalls.php';
class DashboardController extends BaseController
{
	protected $uname;
	protected $log;
	public function init()
	{
		$this->uname=Zend_Auth::getInstance()->getStorage()->read();
		$this->log = Zend_Registry::get('logger');
	}
	public function indexAction()
	{
		$scall=new Model_SOAPCalls();
		$result=$scall->getDashboardItems($this->uname);
		
		$this->_helper->json->sendJson($result);
	}
}