<?php
require_once 'SOAPAuth.php';
class LogoutController extends BaseController {

	public function indexAction() {
		$auth = Zend_Auth::getInstance();
		$auth->clearIdentity();
		Zend_Session::expireSessionCookie();
		Zend_Session::destroy(true);
		$this->_helper->json->sendJson(array('logout'=>true));
	}
}