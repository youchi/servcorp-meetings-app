<?php
class BaseController extends Zend_Controller_Action {
	
	protected function getBootstrap() {
		return $this->getInvokeArg('bootstrap');
	}
	
	protected function getDb() {
		$bootstrap = $this->getBootstrap();
		$resource = $bootstrap->getPluginResource('db');
		$db = $resource->getDbAdapter();
		return $db;
	}

}