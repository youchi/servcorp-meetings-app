

create table `website_session` (
	`SessionId` varchar (96),
	`SavePath` varchar (96),
	`Name` varchar (96),
	`Modified` double ,
	`Lifetime` double ,
	`SessionData` blob 
); 
