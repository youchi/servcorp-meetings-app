

CREATE TABLE tblBookingHolidayRule (
_ID INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
_IDNewDataSet char(1),
CloseAllDay char(4),
CloseDay char(2),
CloseHour char(2),
CloseMinute char(2),
CloseMonth char(2),
CloseYear char(4),
id char(25),
Note char(50),
OpenHour char(2),
OpenMinute char(2),
rowOrder char(4),
SiteID char(5)
);

CREATE TABLE tblBookingWeeklyRule (
_ID INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
_IDNewDataSet char(1),
AfterHour char(2),
AfterMinute char(2),
CloseAllDay char(5),
CloseHour char(2),
CloseMinute char(2),
DayOfWeek char(1),
id char(24),
OpenHour char(1),
OpenMinute char(2),
rowOrder char(4),
SiteID char(5)
);

CREATE TABLE tblCity (
_ID INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
_IDNewDataSet char(1),
CityCode char(4),
CountryCode char(2),
Description char(12),
id char(9),
rowOrder char(2),
TaxRegionCode char(2)
);

CREATE TABLE tblCountry (
_ID INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
_IDNewDataSet char(1),
CalcDay char(2),
CountryCode char(2),
CultureCode char(5),
CurrencyCode char(3),
DefaultLanguage char(1),
Description char(20),
id char(12),
InterestRate char(1),
PortalCode char(1),
rowOrder char(2),
TaxOn char(5)
);

CREATE TABLE tblHottdeskService (
_ID INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
_IDNewDataSet char(1),
ActionLinkCode char(1),
Description char(28),
id char(20),
IsBasicService char(5),
ItemID char(8),
rowOrder char(2),
ServiceID char(2)
);

CREATE TABLE tblHottdeskServiceAvailability (
_ID INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
_IDNewDataSet char(1),
CountryCode char(2),
id char(32),
rowOrder char(2),
ServiceID char(2)
);

CREATE TABLE tblLanguage (
_ID INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
_IDNewDataSet char(1),
id char(12),
LanguageCode char(3),
LanguageID char(1),
LanguageName char(19),
NameDescending char(5),
rowOrder char(1)
);

CREATE TABLE tblLocation (
_ID INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
_IDNewDataSet char(1),
AltDescription char(39),
CityCode char(4),
Description char(40),
id char(13),
LocationCode char(3),
ProxyURL char(81),
rowOrder char(2)
);

CREATE TABLE tblLocationPhoneInfo (
_ID INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
_IDNewDataSet char(1),
AreaPrefix char(3),
CountryPrefix char(4),
ExtensionPrefix char(4),
id char(22),
LocationCode char(3),
rowOrder char(2)
);

CREATE TABLE tblOfficePortal (
_ID INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
_IDNewDataSet char(1),
Description char(16),
FTPURL char(27),
id char(16),
OfficeURL char(22),
PortalCode char(1),
rowOrder char(1)
);

CREATE TABLE tblResource (
_ID INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
_IDNewDataSet char(1),
Complimentable char(5),
GroupID char(1),
id char(15),
ItemIDAfterHour char(8),
ItemIDAfterMin char(8),
ItemIDComp char(8),
ItemIDFlatRate char(8),
ItemIDUnit char(8),
ItemIDWeekEnd char(8),
ResourceID char(4),
ResourceName char(100),
ResourceTypeID char(1),
rowOrder char(4),
SiteID char(5),
Status char(1),
Username char(15)
);

CREATE TABLE tblResourceType (
_ID INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
_IDNewDataSet char(1),
Description char(25),
id char(16),
ResourceTypeID char(1),
rowOrder char(1),
SecretaryService char(5)
);

CREATE TABLE tblSite (
_ID INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
_IDNewDataSet char(1),
AccountName char(58),
AccountNo char(26),
AccountSummary char(5),
AccountType char(29),
AddressLine1 char(55),
AddressLine2 char(56),
AddressLine3 char(36),
AltAccountName char(52),
AltAccountType char(29),
AltAddressLine1 char(42),
AltAddressLine2 char(56),
AltAddressLine3 char(42),
AltBankName char(56),
AltBranchName char(64),
AltDescription char(44),
AltInvoiceNote char(33),
AltSiteName char(42),
BankName char(56),
BranchName char(64),
BranchNumber char(20),
BSBNumber char(20),
CallBilling char(5),
CallBillingLimit char(12),
CommonSpace char(8),
CompanyID char(10),
Description char(45),
Email char(40),
Fax char(17),
HottdeskCode char(3),
HottdeskVersion char(3),
id char(10),
InternetBilling char(5),
InternetBillingLimit char(12),
InvoiceNote char(69),
LocationCode char(3),
MerchantID char(13),
Phone char(17),
PostCode char(8),
ReportType char(1),
rowOrder char(3),
ServcorpSpace char(1),
SiteID char(5),
SiteName char(46),
SiteType char(1),
SwiftCode char(25),
TaxOnTotal char(5),
TerminalID char(8),
WebPage char(26),
tblSite_space char(8)
);

CREATE TABLE tblTaskTimerGroup (
_ID INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
_IDNewDataSet char(1),
Description char(31),
GroupID char(1),
id char(18),
rowOrder char(1),
Title char(4)
);

CREATE TABLE view_HottdeskService (
_ID INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
_IDNewDataSet char(1),
CurrencyCode char(3),
Description char(26),
id char(23),
ItemID char(8),
Price char(11),
rowOrder char(3),
ServiceID char(2),
SiteID char(5)
);

alter table tblCountry modify _IDNewDataSet char(1) default '1';
alter table tblCountry modify CalcDay char(2) default '23';
alter table tblCity modify _IDNewDataSet char(1) default '1';
alter table tblLocation modify _IDNewDataSet char(1) default '1';
alter table tblResource modify _IDNewDataSet char(1) default '1';
alter table tblResourceType modify _IDNewDataSet char(1) default '1';
alter table tblSite modify _IDNewDataSet char(1) default '1';
alter table tblHottdeskService modify _IDNewDataSet char(1) default '1';
alter table tblHottdeskServiceAvailability modify _IDNewDataSet char(1) default '1';
alter table tblLanguage modify _IDNewDataSet char(1) default '1';
alter table tblLocationPhoneInfo modify _IDNewDataSet char(1) default '1';
alter table tblOfficePortal modify _IDNewDataSet char(1) default '1';
alter table tblTaskTimerGroup modify _IDNewDataSet char(1) default '1';
alter table view_HottdeskService modify _IDNewDataSet char(1) default '1';
