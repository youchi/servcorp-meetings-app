<?php
define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../../application'));
include_once(APPLICATION_PATH . '/utils/Translator.php');

$translater = new Translator();

Header("content-type: application/x-javascript");
?>
(function() {
	window.servcorp = window.servcorp || {};
	
	$(document).ready(function(){
		$('#call1,#call').on('click','a',function(){
			window.open('tel:'+$("#call1").html().replace(/ /g,''));
			});
		$('#email,#email1').on('click','a',function(){
			window.open('mailto:'+servcorp.utility.email);
			});

		$('#id_phone').on('click','a',function() {
			window.open('tel:'+($('#id_phone span').text()).replace(/ /g,''));
		});
		$('#id_fax').on('click','a',function() {
			window.open('tel:'+($('#id_fax').text()).replace(/ /g,''));
		});
		$("#id_maplink").on("click",'a',function() { 
			window.open(servcorp.booking.mapsrc['imap']);
		});
		$('#password,#username').on('keypress','input', function(e)
				{
			if(navigator.appVersion.indexOf("iPhone")!=-1){
				     if(e.keyCode == 10)
				     {
				    	 e.preventDefault();
				    	 $('#id_login').click();
				     }
			}
			else
				if(e.keyCode == 13)
			     {
			    	 $('#id_login').click();
			     }
				});
				
	});

	servcorp.ajax = (function() {
		var bookedItems;
		var enLoader=false;
		return {
			login : function() {
				$('#login_error_msg').hide();
				$.mobile.loading('show', {
				    theme: "d",
				});
				//empty username and password
				if($('#username').val()=='' && $('#password').val()==''){
					$.mobile.loading('hide');
					$('#login_error_msg').html("<?=$translater->translate("Please enter your username and password.")?>");
					$('#login_error_msg').show();
				}
				else if($('#username').val()==''){
					$.mobile.loading('hide');
					$('#login_error_msg').html("<?=$translater->translate("Please enter your username.")?>");
					$('#login_error_msg').show();
				}
				else if($('#password').val()==''){
					$.mobile.loading('hide');
					$('#login_error_msg').html("<?=$translater->translate("Please enter your password.")?>");
					$('#login_error_msg').show();
				}
				else
				$.ajax( {
					type : 'POST',
					url : BASEURL+'login',
					dataType : 'json',
					data : 'username=' + $('#username').val() + '&password='
							+ $('#password').val()+'&rememberme='+$('#rememberme').is(':checked'),
					success : function(data) {
						$.mobile.loading('hide');
						if (data.login == true) {
							$('#login_error_msg').hide();
							servcorp.ajax.dashboard();
							
							$('#username').val('');
							$('#password').val('');
						} else {

							$('#username').val('');
							$('#password').val('');
							htmlStr="<?=$translater->translate("Invalid username or password.")?>";
							$('#login_error_msg').html(htmlStr);
							$('#login_error_msg').show();
						}
					},
					error : function(XMLHttpRequest, textStatus, errorThrown) {
						$('#username').val('');
						$('#password').val('');
						$.mobile.loading('hide');
						//alert(XMLHttpRequest.status);
	                    //alert(XMLHttpRequest.responseText );
						servcorp.utility.reset();
					}
				});
			},
			
			logout	:	function() {
				$.ajax( {
					type : 'POST',
					url : BASEURL+'logout',
					dataType : 'json',
					success : function(data) {
						servcorp.utility.defaultBookingValues();
						//window.location.reload();
						$.mobile.changePage('#home', { transition: "slidedown" });
					},
					error : function(XMLHttpRequest, textStatus, errorThrown) {
						servcorp.utility.reset();
					}
				});
			},
			
			dashboard: function() {
				$.mobile.loading('show', {
				    theme: "d",
				});

				$('#logout').on('click','', function(){
					servcorp.ajax.logout();
				});

				$('ul.detailList').html("<li><a style='text-align:center;'><span><?=$translater->translate("Refreshing bookings...")?></span></a></li>");
	
				$.ajax( {
					type : 'GET',
					url : BASEURL+'dashboard',
					dataType : 'json',
					success : function(data) {
						// TODO the json response list should be stored as an
						// object which will be accessible by editbooking
						// function
						servcorp.booking.bitems = data;
						var htmlStr ="";
						console.log('returned');
						console.log(data);
						for(i=0;i<servcorp.booking.bitems.length;i++) {
							htmlStr+=servcorp.utility.createBookedItemElement(i,servcorp.booking.bitems[i]);
						}
						$('ul.detailList').html(htmlStr?htmlStr:"<li><a style='text-align:center;'><span><?=$translater->translate("Currently you have no future bookings.")?></span></a></li>");
						
						$(document).off("click", "a.detailBookingBtn");
						$('a.detailBookingBtn').on('click','', function(){
							servcorp.ajax.detailBooking(this.id.replace('detailBookingBtn', ''));
						});

						$(document).off("click", "#dashboardBookingBtn");

						$('#dashboardBookingBtn').on('click','',function() {
							$('#cancel_bt').prop("href", "#invite_colleagues");
							$('#select_date_back').prop("href", "#select_resource");
							$('#booking_head').text('<?=$translater->translate("New Booking")?>');
							servcorp.ajax.getBookingCountryList();
						});
						if($.mobile.activePage.attr('id')!="dashboard"){
							$.mobile.changePage('#dashboard', {transition: 'slideup'});	
						}
						$
						servcorp.utility.defaultBookingValues();
						$.mobile.loading('hide');
					},
					error : function(XMLHttpRequest, textStatus, errorThrown) {
						servcorp.utility.reset();

					}
				});

			},
			
			getBookingCountryList : function() {

				$.mobile.loading('show', {
				    theme: "d",
				});


				$('#countrylist').html("<li><a style='text-align:center;'><span class='fntsml_sty'><?=$translater->translate("Refreshing country list...")?></span></a></li>");

					
			        $.ajax({
					type  	 : 'GET',
					url	  	 : BASEURL+'booking',
					dataType : 'json',
					data	 : 'value=country',
					success	 : function(data) {
						htmlStr = servcorp.utility.setBookingCountryList(data);
						$('#countrylist').html(htmlStr);

						$(document).off("click", "h5.countryname");
						$('h5.countryname').on('click','',function(){
							servcorp.ajax.getBookingLocationList($(this).attr('name'),$(this).text());
						});
						
						$.mobile.changePage('#select_country');
					},
					error	 : function(XMLHttpRequest, textStatus, errorThrown) {
						$.mobile.loading('hide');
						servcorp.utility.reset();
						
					}
						
				});
			    
				
			},
			
			getBookingLocationList	  : function(countrycode,countryname) {
				$.mobile.loading('show', {
				    theme: "d",
				});
				servcorp.utility.countrycode=countrycode;
				servcorp.booking.setCountry(countryname);
				$('#citylist').html("<li><a style='text-align:center;'><span class='fntsml_sty'><?=$translater->translate("Refreshing location list...")?></span></a></li>");
				$.ajax({
					type  	 : 'GET',
					url	  	 : BASEURL+'booking',
					dataType : 'json',
					data	 : 'value=city&country='+countrycode,
					success	 : function(data) {
						htmlStr = servcorp.utility.setBookingLocationList(data);
						$('#citylist').html(htmlStr);
						$(document).off("click", "h5.locationname");
						$('h5.locationname').on('click','',function(){
							servcorp.ajax.getBookingResourceTypes($(this).attr('name'),$(this).text());
						});
						$(document).one('change')
						$.mobile.changePage('#select_location', { transition: 'slideup' });
						$.mobile.loading('hide');
					},
					error	 : function(XMLHttpRequest, textStatus, errorThrown) {
						$.mobile.loading('hide');
						servcorp.utility.reset();
					}
				});
			},
			
			getBookingResourceTypes	:	function(locationcode,locationname) {
				$.mobile.loading('show', {
				    theme: "d",
				});
				servcorp.booking.setAddress(locationname);
				$('#RTlist').html("<li><a style='text-align:center;'><span class='fntsml_sty'><?=$translater->translate("Refreshing resource types...")?></span></a></li>");
				$.ajax({
					type  	 : 'GET',
					url	  	 : BASEURL+'booking',
					dataType : 'json',
					data	 : 'value=resourcetype&location='+locationcode,
					success	 : function(data) {
					console.log(data);
						htmlStr = servcorp.utility.setBookingResourceTypes(data);
						$('#RTlist').html(htmlStr);
						$(document).off("click", "h5.rtname");
						$('h5.rtname').on('click','',function(){
							servcorp.ajax.getBookingResources($(this).attr('name'),$(this).text(),locationcode);
						});
						$.mobile.changePage('#select_resource_type', { transition: 'slideup' });
						$.mobile.loading('hide');
					},
					error	 : function(XMLHttpRequest, textStatus, errorThrown) {
						$.mobile.loading('hide');
						servcorp.utility.reset();
					}
				});
			},
			
			getBookingResources	:	function(RTid,RTname,locationcode) {
				$.mobile.loading('show', {
				    theme: "d",
				});
				servcorp.booking.setResourceType(RTname);
				$('#Rlist').html("<li><a style='text-align:center;'><span class='fntsml_sty'><?=$translater->translate("Refreshing resources list...")?></span></a></li>");
				$.ajax({
					type  	 : 'GET',
					url	  	 : BASEURL+'booking',
					dataType : 'json',
					data	 : 'value=resource&location='+locationcode+'&RTid='+RTid,
					success	 : function(data) {
					
						htmlStr=servcorp.utility.setBookingResources(data);
						$('#Rlist').html(htmlStr);
						$(document).off("click", "h5.rname");
						$('h5.rname').on('click','',function(){
							servcorp.booking.setResource($(this).text(),$(this).attr('name'));
						});
						$.mobile.changePage('#select_resource', { transition: 'slideup' });
						
						$.mobile.loading('hide');
					},
					error	 : function(XMLHttpRequest, textStatus, errorThrown) {
						$.mobile.loading('hide');
						servcorp.utility.reset();
					}
				});
			},
			
			getTimeSlot		:	function(date) {
				$.mobile.loading('show', {
				    theme: "d",
				});
				$.ajax({
					type  	 : 'GET',
					url	  	 : BASEURL+'booking',
					dataType : 'json',
					data	 : 'value=timeslot&date='+date+'&resourceid='+servcorp.booking.resourceID,
					success	 : function(data) {
						servcorp.utility.email=data[0]['Email'];
						$("#call,#call1").html(data[0]['Phone']);
						for(var slot in data) {
							if(data[slot]['StartHour'] && data[slot]['StartHour']==Number(servcorp.booking.s_hr) && data[slot]['EndHour']==Number(servcorp.booking.e_hr) && data[slot]['StartMin']==Number(servcorp.booking.s_min) && data[slot]['EndMin']==Number(servcorp.booking.e_min))
						  data.splice(slot,1);
						}
						servcorp.utility.busyslots = data;


						$.mobile.loading('hide');
					},
					error	 : function(XMLHttpRequest, textStatus, errorThrown) {
						$.mobile.loading('hide');
						servcorp.utility.busyslots = {};
					}
				});
			},
			getEmailPhoneForDate :function(){
				$.ajax({
					type  	 :'GET',
					url   	 : BASEURL+'booking',
					dataType :'json',
					data     :'value=date&resourceid='+servcorp.booking.resourceID,
					success  : function(data){
						servcorp.utility.email=data[0]['Email'];
						$("#call,#call1").html(data[0]['Phone']);
					},
					error :function(XMLHttpRequest,textStatus,errorThrown){
						$.mobile.loading('hide');
					}
				});
			},
			
			createBooking : function () {
				var count=$('#done_booking').attr('name');
				if(count!='') {
					servcorp.ajax.updateBooking(count);
				}
				else {
					var jObj = servcorp.booking.getBookingDetails();
					$.mobile.loading('show', {
				    theme: "d",
				});
					if(jObj['e_min'] && jObj['resource'] && jObj['date'])
						$.ajax( {
							type : 'POST',
							url	 : BASEURL+'booking',
							dataType : 'json',
							data : 'resourceid='+jObj['resourceid']+'&s_hr='+jObj['s_hr']+'&s_min='+jObj['s_min']+'&e_hr='+jObj['e_hr']+'&e_min='+jObj['e_min']+'&date='+jObj['date']+'&comments='+jObj['comments']+'&invitees='+jObj['invitees'],
							success : function(data) {
								$.mobile.loading('hide');
								if(data.refno=='0')
									alert('<?=$translater->translate("This resource has been fully booked for the time selected. Please choose a different time or resource.")?>');
								else if(data.refno=='1')
									alert('<?=$translater->translate("Please contact Servcorp Staff for booking on weekends / public holidays.")?>');
								else if(data.refno=='2')
									alert('<?=$translater->translate("The time you selected for this resource is outside its available hours. Please contact a Servcorp team member to make this booking.")?>');
								else {
									alert('<?=$translater->translate("Thank you for making a booking. Your booking has been confirmed.")?>');
									servcorp.booking.setDefaultValues();
									servcorp.ajax.dashboard();
									$.mobile.changePage('#dashboard', { transition: 'slideup' });
									$.mobile.navigate.history.clearForward;
								}
							},
							error	: function(XMLHttpRequest, textStatus, errorThrown) {
								servcorp.utility.reset();
							}
						} );
					else {
						$.mobile.loading('hide');
						alert('<?=$translater->translate("Please fill in the details.")?>');
					}
				}
			},
			
			detailBooking	:	function(count) {
				// TODO set details from the list which will have the values
				// from json resp object assigned from the dashboard method
				$.mobile.loading('show', {
				    theme: "d",
				});
				servcorp.utility.defaultBookingValues();
				servcorp.booking.bitems[count].location = servcorp.booking.bitems[count].location.replace('<br />',', ');
                $('#details_title').text(servcorp.booking.bitems[count].location);
				$('#details_location').text(servcorp.booking.bitems[count].location+', '+servcorp.booking.bitems[count].country);
				$('#id_phone span').text(servcorp.booking.bitems[count].phone);
				$('#id_fax span').text(servcorp.booking.bitems[count].fax);
				$('#id_date').text(servcorp.booking.bitems[count].date);
				$('#id_time').text(' '+servcorp.booking.bitems[count].stime+' ~ '+servcorp.booking.bitems[count].etime);
				$('#delete_booking').attr('name',servcorp.booking.bitems[count].refno);
				servcorp.booking.setMap(servcorp.booking.bitems[count].latitude,servcorp.booking.bitems[count].longitude);
				$('#id_editbooking').show();
				$('#id_tdybooking').hide();
				var today=new Date();
				today.setHours(0, 0, 0, 0);
				var booked=new Date(servcorp.booking.bitems[count].date);
				if((booked-today)==0){
					$('#id_editbooking').hide();
					$('#id_tdybooking').show();
				}
				$(document).off("click", "#id_editbooking");
				$('#id_editbooking').on('click', '', function(){
					servcorp.ajax.editBooking(count);
				});
				$(document).off("", "#delete_booking");
				$('#delete_booking').on('click', '', function(){
					var con = confirm('<?=$translater->translate("Are you sure you wish to delete this booking?")?>');
					if(con)
						servcorp.ajax.deleteBooking($(this).attr('name'));
				});
					$.mobile.changePage('#details1', { transition: 'slide' });
				$.mobile.loading('hide');
			},
			
			editBooking		: 	function(count) {
				$('#booking_head').text('<?=$translater->translate("Edit Booking")?>');
				servcorp.utility.defaultBookingValues(servcorp.booking.bitems[count]);
				$('#cancel_bt').prop("href", "#dashboard");
				$('#select_date_back').prop("href", "#details1");

					$.mobile.changePage('#new_booking', { transition: 'slideup' });

				$('#done_booking').attr('name',count);
			},
			
			updateBooking	:	function(count) {
				$.mobile.loading('show', {
				    theme: "d",
				});
				var jObj = servcorp.booking.getTime();
				var date = servcorp.booking.getDate();
				var comments=servcorp.booking.getComments();
				var invitees=servcorp.booking.getInvitees();
				if(jObj['s_hr']==servcorp.booking.bitems[count].stime.split(':')[0] &&
						jObj['s_min']==servcorp.booking.bitems[count].stime.split(':')[1] && 
						jObj['e_hr']==servcorp.booking.bitems[count].etime.split(':')[0] &&
						jObj['e_min']==servcorp.booking.bitems[count].etime.split(':')[1] &&
						date['date']==servcorp.booking.bitems[count].date &&
						comments['comments']==servcorp.booking.bitems[count].comments &&
						invitees['invitees']==''){
					$.mobile.loading('hide');
					$('#done_booking').attr('name',count);
						$.mobile.changePage('#details1', { transition: 'slide' });
					
				}
				else
					$.ajax( {
						type : 'POST',
						url	 : BASEURL+'booking/edit',
						dataType : 'json',
						data : 'refno='+servcorp.booking.bitems[count].refno+'&s_hr='+jObj['s_hr']+'&s_min='+jObj['s_min']+'&e_hr='+jObj['e_hr']+'&e_min='+jObj['e_min']+'&date='+date['date']+'&resourceid='+servcorp.booking.bitems[count].resourceid+'&comments='+comments['comments']+'&invitees='+invitees['invitees'],
						success : function(data) {
							$.mobile.loading('hide');
							if(data.result=='0'){
								alert('<?=$translater->translate("This resource has been fully booked for the time selected. Please choose a different time or resource.")?>');
								$('#done_booking').attr('name',count);
							}
							else if(data.result=='1')
								alert('<?=$translater->translate("Please contact Servcorp Staff for booking on weekends / public holidays.")?>');
							else if(data.result=='2')
								alert('<?=$translater->translate("The time you selected for this resource is outside its available hours. Please contact a Servcorp team member to make this booking.")?>');
							else { 
								alert('<?=$translater->translate("Your booking has been updated.")?>');
								servcorp.booking.setDefaultValues();
								servcorp.ajax.dashboard();
								$('#done_booking').attr('name','');
								$( document ).one( "pagechange", function () {
									$.mobile.changePage('#dashboard', { transition: 'slideup' });
								})

							}
						},
						error	: function(XMLHttpRequest, textStatus, errorThrown) {
							$.mobile.loading('hide');
							servcorp.utility.reset();
						}
					} );
			},
			
			deleteBooking	:	function(refno) {
				$.mobile.loading('show', {
				    theme: "d",
				});
				$.ajax( {
					type : 'POST',
					url	 : BASEURL+'booking/delete',
					dataType : 'json',
					data : 'refno='+refno,
					success : function(data) {
						
						if(data.result)
							alert('<?=$translater->translate("Booking deleted")?>');
						else
							alert('<?=$translater->translate("Delete booking failed, please try again")?>');
						
						
						servcorp.booking.setDefaultValues();
						servcorp.ajax.dashboard();
						$('#done_booking').attr('name','');
						$( document ).one( "pagechange", function () {
							$.mobile.changePage('#dashboard_2', { transition: 'slideup' });
							});
					},
					error	: function(XMLHttpRequest, textStatus, errorThrown) {
						$.mobile.loading('hide');
						servcorp.utility.reset();
					}
				} );
			},
			
			cancelBooking	:	function() {
				servcorp.utility.defaultBookingValues();
				servcorp.ajax.dashboard();
			}
		}
	})();
	
	servcorp.booking = (function() {
		var country,address,resourceType,resource,resourceID,date,s_hr,s_min,e_hr,e_min,comments,invitees,mapsrc;
		return {
			setCountry : function (country) {
				this.country=country;
				$('#mb_country').text(country);
				$( document ).one( "pagechange", function () {
					$.mobile.changePage('#select_location', { transition: 'slide' });
				});
			}, 
			setAddress : function (address) {
				this.address=address;
				$('#mb_location').text(address);
				$( document ).one( "pagechange", function () {
					$.mobile.changePage('#select_resource_type', { transition: 'slide' });
				});
				//alert($('#select_location').css('display'));
			},
			setResourceType : function (resourceType) {
				$.mobile.loading('show', {
				    theme: "d",
				});
				this.resourceType = resourceType;
				$('#mb_resource_type').text(resourceType);
				$( document ).one( "pagechange", function () {
					$.mobile.changePage('#select_resource', { transition: 'slide' });
				});
			},
			setResource : function (resource,resourceID) {
				$.mobile.loading('show', {
				    theme: "d",
				});
				this.resource = resource;
				this.resourceID = resourceID;
				$('#mb_resource').text(resource);
				$('#mb_resource').attr('name',resourceID)

					//setTimeout("$.mobile.changePage('#select_date', { transition: 'slideup' });",1000);
					$.mobile.changePage('#select_date', { transition: 'slide' });

				servcorp.utility.setEmailPhone(true);
				$.mobile.loading('hide');
			},
			setDate : function (date,past_date) {
				$.mobile.loading('show', {
				    theme: "d",
				});
				this.date = date;
				$('#mb_date').text(date);
				$.mobile.changePage('#select_time', { transition: 'slide' });
				if(typeof(past_date) != "undefined")
					servcorp.utility.setTimeOptions(true,past_date);
				else
					servcorp.utility.setTimeOptions(true);
				$.mobile.loading('hide');
			},
			setTime : function (s_hr,s_min,e_hr,e_min) {
				this.s_hr=s_hr;this.s_min=((s_min==0)?'00':s_min);this.e_hr=e_hr;this.e_min=((e_min==0)?'00':e_min);
				$('#mb_time').text('from '+this.s_hr+':'+this.s_min+' to '+this.e_hr+':'+this.e_min);
					$.mobile.changePage('#additional_requirements', { transition: 'slide' });
			},
			setRequirements : function(){
				this.comments=$('#requirements').val();
				$.mobile.changePage('#invite_colleagues', { transition: 'slide' });
			},
			setInvitees : function(){
				this.invitees=$('#invitees').val();
				$.mobile.changePage('#new_booking', { transition: 'slide' });
			},
			getDate	:	function () {
				return {
					'date':this.date,
				};
			},
			getTime	: function() {
				return {
					's_hr':this.s_hr,
					's_min':this.s_min,
					'e_hr':this.e_hr,
					'e_min':this.e_min,
				};
			},
			getComments :function(){
				return{
					'comments':this.comments,
				}
			},
			getInvitees : function(){
				return{
					'invitees':this.invitees,
				}
			},
			getBookingDetails : function() {
				return {
						'country':this.country,
						'address':this.address,
						'resourceType':this.resourceType,
						'resource':this.resource,
						'date':this.date,
						's_hr':this.s_hr,
						's_min':this.s_min,
						'e_hr':this.e_hr,
						'e_min':this.e_min,
						'date':this.date,
						'resourceid':this.resourceID,
						'comments':this.comments,
						'invitees':this.invitees,
						};
			},
			setDefaultValues	:	function() {
				this.country=this.address=this.resourceType=this.resource=this.resourceID=this.date=this.s_hr=this.s_min=this.e_hr=this.e_min=this.comments=this.invitees='';
			},
			setMap : function(latitude,longitude) {
				servcorp.booking.mapsrc=servcorp.utility.getMapUrl(latitude,longitude);
				$("#id_map").attr("src",servcorp.booking.mapsrc['static']);
			}
			
		}
	})();
	
	servcorp.utility = (function() {
		var busyslots,email,countrycode;
		var months = ['<?=$translater->translate("DEC")?>', '<?=$translater->translate("JAN")?>', '<?=$translater->translate("FEB")?>', '<?=$translater->translate("MAR")?>', '<?=$translater->translate("APR")?>', '<?=$translater->translate("MAY")?>', '<?=$translater->translate("JUN")?>', '<?=$translater->translate("JUL")?>', '<?=$translater->translate("AUG")?>', '<?=$translater->translate("SEP")?>', '<?=$translater->translate("OCT")?>', '<?=$translater->translate("NOV")?>', '<?=$translater->translate("DEC")?>', '<?=$translater->translate("JAN")?>'];
		var setButtonText = function(month){
			$('.ui-icon-circle-triangle-e').html(months[month+1]);
			$('.ui-icon-circle-triangle-w').html(months[month-1]);
		};
		return {
			getMapUrl	:	function(latitude,longitude) {
				address = latitude+','+longitude;
				return {'static':'http://maps.google.com/maps/api/staticmap?center='+address+'&zoom=16&size=280x165&sensor=false&markers=color:red|'+address+'&mobile=true',
					'imap':'http://maps.google.com/maps?q='+address};
			},
			createBookedItemElement	:	function(it,item) {
				// Item shown on dashboard
				var dashboardItemHtml = "<li class=\"arrow\"><a href=\"javascript:void(0);\" id=\"detailBookingBtn{loop_counter}\" class=\"detailBookingBtn\"> <img src=\"img/iMeeting.png\" class=\"imeeting\"><h3 >"+
				"<span id=\"id_country{loop_counter}\">{COUNTRY}</span><br /><em id=\"id_location{loop_counter}\" class=\"truncate_text\">{LOCATION}</em></h3>"+
				"{DATE} @ <span id=\"id_time{loop_counter}\">{TIME}</span></a></li>";
				item.location = item.location.replace(', ','<br />');
				return dashboardItemHtml.
						replace(/{COUNTRY}/g,item.country).
						replace(/{LOCATION}/g,item.location).
						replace(/{DATE}/g,item.date).
						replace(/{TIME}/g,item.stime+' - '+item.etime).
						replace(/{loop_counter}/g,it);
			},
			setBookingCountryList : function(items) {
				var newBookingCountry = '<li class="countrylist arrow">'+
		    							'<a href="javascript:void(0);"><h5 class="countryname" id="country{COUNTRY_ID}" name="{COUNTRY_CODE}">{COUNTRY}</h5></a></li>';
				var counHtml = "";

				// set jp to first element if its for jp
				if (window.location.host.toString().substr(-2) == 'jp'){
					counHtml = '<li class="arrow"><a class="ui-link" href="javascript:void(0);"><h5 id="country19" class="countryname" name="JP">日本</h5></a></li>';
				}

				for(i in items.country) {
					if(items.country[i]['Description'] != ""){
						//counHtml+=newBookingCountry.replace(/{COUNTRY_ID}/g,i).replace(/{COUNTRY}/g,items.country[i]['Description']).replace(/{COUNTRY_CODE}/g,items.country[i]['CountryCode']);
						//counHtml += '<li class="countrylist arrow"><h5 class="countryname" style="color:#111111; font-size:16px;line-height:20px;margin:-2px 0 2px;	text-shadow:0 1px 0 #FFFFFF;" id="country' + i + '" name="' + items.country[i]['CountryCode'] + '">' + items.country[i]['Description'] + '</h5></li>';
						counHtml += '<li class="arrow"><a href="javascript:void(0);"><h5 class="countryname" id="country' + i + '" name="' + items.country[i]['CountryCode'] + '">' + items.country[i]['Description'] + '</h5></a></li>';
					}
				}
				return counHtml
			},
			setBookingLocationList	: function(items) {
				var newBookingLocation = '<li class="arrow">'+
									    	'<a href="javascript:void(0);"><h5 class="locationname" id="location{LOCATION_ID}" name="{LOCATION_CODE}">{LOCATION}</h5></a>'+
									    '</li>';
				var locHtml="";
				for (i in items) 
					locHtml+=newBookingLocation.replace(/{LOCATION_ID}/g,i).replace(/{LOCATION_CODE}/g,items[i]['LocationCode']).replace(/{LOCATION}/g,items[i]['Description']);
				return ((locHtml)? locHtml:"<li><a style='text-align:center;'><span><?=$translater->translate("No resources available for this location")?></span></a></li>");
			},
			setBookingResourceTypes	:	function(items) {
				var newBookingRT = '<li class="arrow">'+
								    	'<a href="javascript:void(0);"><h5 class="rtname" id="rt{RT_ID}" name="{RT_CODE}">{RT}</h5></a>'+
								    '</li>';
				var rtHtml="";
				for (i in items)
					rtHtml+=newBookingRT.replace(/{RT_ID}/g,i).replace(/{RT_CODE}/g,items[i]['ResourceTypeID']).replace(/{RT}/g,items[i]['Description']);
				return ((rtHtml)? rtHtml:"<li><a style='text-align:center;'><span><?=$translater->translate("No resources available")?></span></a></li>");
			},
			setBookingResources		:	function(items) {
				var newBookingR = '<li class="arrow">'+
								    	'<a href="javascript:void(0);"><h5 class="rname" id="r{R_ID}" name="{R_CODE}">{R}</h5></a>'+
								    '</li>'
				var rHtml="";
				for (i in items) {
					if(items[i]['ResourceName'])
						rHtml+=newBookingR.replace(/{R_ID}/g,i).replace(/{R_CODE}/g,items[i]['ResourceID']).replace(/{R}/g,items[i]['ResourceName']);
				}
				return rHtml;
			},
			defaultBookingValues	:	function(bitems) {
				$('#mb_country').text((typeof(bitems) != "undefined")? bitems.country:'<?=$translater->translate("Select Country")?>');
				$('#mb_location').text((typeof(bitems) != "undefined")? bitems.location:'<?=$translater->translate("Select City")?>');
				$('#mb_resource_type').text((typeof(bitems) != "undefined")? bitems.resourcetype:'<?=$translater->translate("Select Resource Type")?>');
				$('#mb_resource').text((typeof(bitems) != "undefined")? bitems.resource:'<?=$translater->translate("Select Resource")?>');
				$('#mb_date').text((typeof(bitems) != "undefined")? bitems.date:'<?=$translater->translate("Select Date")?>');
				$('#mb_time').text((typeof(bitems) != "undefined")? bitems.stime+' ~ ' +bitems.etime:'<?=$translater->translate("Select Time")?>');
				$('#requirements').text((typeof(bitems) !="undefined")?bitems.comments:'');
				$('#invitees').text('');
				$(document).off("click", '#a_country');
				$(document).off("click", '#a_location');
				$(document).off("click", '#a_resource_type');
				$(document).off("click", '#a_resource');
				$(document).off("click", '#a_date');
				$(document).off("click", '#a_time');
				var months = ['<?=$translater->translate("DEC")?>', '<?=$translater->translate("JAN")?>', '<?=$translater->translate("FEB")?>', '<?=$translater->translate("MAR")?>', '<?=$translater->translate("APR")?>', '<?=$translater->translate("MAY")?>', '<?=$translater->translate("JUN")?>', '<?=$translater->translate("JUL")?>', '<?=$translater->translate("AUG")?>', '<?=$translater->translate("SEP")?>', '<?=$translater->translate("OCT")?>', '<?=$translater->translate("NOV")?>', '<?=$translater->translate("DEC")?>', '<?=$translater->translate("JAN")?>']
				setTimeout(function(){setButtonText($(".datepicker").datepicker('getDate').getMonth() + 1);}, 0);
				if(typeof(bitems) != "undefined") {
					$('#s_hour').val(bitems.stime);
					$('#duration').val(servcorp.utility.findDuration(bitems.stime,bitems.etime));
					var s_hr=bitems.stime.split(':')[0];
					var s_min=bitems.stime.split(':')[1];
					var e_hr=bitems.etime.split(':')[0];
					var e_min=bitems.etime.split(':')[1];
					//alert(s_hr);alert(s_min);
					servcorp.booking.s_hr=s_hr;
					servcorp.booking.s_min=((s_min==0)?'00':s_min);
					servcorp.booking.e_hr=e_hr;
					servcorp.booking.e_min=((e_min==0)?'00':e_min);
					$('#time_suggest').html('<ul class="itemDetails"><li  class="arrow"><a href="javascript:void(0);" onclick="servcorp.booking.setTime('+s_hr+','+((s_min==0)?'00':s_min)+','+e_hr+','+((e_min==0)?'00':e_min)+');">'+s_hr+':'+((s_min==0)?'00':s_min)+' to '+e_hr+':'+((e_min==0)?'00':e_min)+'</a></li></ul>');
					servcorp.booking.date=bitems.date;
					servcorp.booking.comments=bitems.comments;
					servcorp.booking.invitees='';
					$('.datepicker').datepicker("setDate",new Date(bitems.date));
					$('.datepicker').datepicker({ 
					<? if ($translater->lang == 'en') { ?>
						dateFormat:'dd M yy',
					<? } elseif ($translater->lang == 'ja') { ?>
		closeText: '閉じる',
		prevText: '&#x3c;前',
		nextText: '次&#x3e;',
		currentText: '今日',
		monthNames: ['1月','2月','3月','4月','5月','6月',
		'7月','8月','9月','10月','11月','12月'],
		monthNamesShort: ['1月','2月','3月','4月','5月','6月',
		'7月','8月','9月','10月','11月','12月'],
		dayNames: ['日曜日','月曜日','火曜日','水曜日','木曜日','金曜日','土曜日'],
		dayNamesShort: ['日','月','火','水','木','金','土'],
		dayNamesMin: ['日','月','火','水','木','金','土'],
		weekHeader: '週',
		dateFormat: 'yy/mm/dd',
		firstDay: 0,
		isRTL: false,
		showMonthAfterYear: true,
		yearSuffix: '年',
					<? } ?>
						minDate: new Date(),
						prevText: ' ',
						nextText: ' ',
						onSelect: function(dateText, inst) {
						setTimeout(function(){setButtonText($(".datepicker").datepicker('getDate').getMonth() + 1);}, 0);
							servcorp.booking.setDate(dateText,bitems.date);
						},
						onChangeMonthYear: function(year, month, inst) {
							setTimeout(function(){setButtonText(month);}, 0);
						}
					});
					
					$('#a_country').on('click','', function(){ alert('<?=$translater->translate("You can only edit date and time")?>'); });
					$('#a_location').on('click', '', function(){ alert('<?=$translater->translate("You can only edit date and time")?>'); });
					$('#a_resource_type').on('click', '', function(){ alert('<?=$translater->translate("You can only edit date and time")?>'); });
					$('#a_resource').on('click', '', function(){ alert('<?=$translater->translate("You can only edit date and time")?>'); });
					$('#a_time').on('click', '', function(){ 
							$.mobile.changePage('#select_time', { transition: 'slide' });
						/*$('#s_hour, #duration').die();*/ 
						servcorp.utility.setTimeOptions(true,bitems.date); 
					});
					$('#a_date').on('click', '', function(){ 
						setTimeout(function(){setButtonText($(".datepicker").datepicker('getDate').getMonth() + 1);}, 0);

							$.mobile.changePage('#select_date', { transition: 'slide' });

						servcorp.utility.setEmailPhone(true); 
					});
					$('#arr_cnt').attr('class','');
					$('#arr_loc').attr('class','');
					$('#arr_rt').attr('class','');
					$('#arr_r').attr('class','');
					servcorp.booking.resourceID=bitems.resourceid;
				} else {
					$(function(){
						// Datepicker - //To set date -
						// $('.datepicker').datepicker( "setDate" , '08/29/2010'
						// );
						//To set current date
						$('.datepicker').datepicker("setDate",new Date());
						$('.datepicker').datepicker({ /*beforeShowDay: $.datepicker.noWeekends,*/
							minDate: new Date(),
						<? if ($translater->lang == 'en') { ?>
							dateFormat:'dd M yy',
						<? } elseif ($translater->lang == 'ja') { ?>
		closeText: '閉じる',
		prevText: '&#x3c;前',
		nextText: '次&#x3e;',
		currentText: '今日',
		monthNames: ['1月','2月','3月','4月','5月','6月',
		'7月','8月','9月','10月','11月','12月'],
		monthNamesShort: ['1月','2月','3月','4月','5月','6月',
		'7月','8月','9月','10月','11月','12月'],
		dayNames: ['日曜日','月曜日','火曜日','水曜日','木曜日','金曜日','土曜日'],
		dayNamesShort: ['日','月','火','水','木','金','土'],
		dayNamesMin: ['日','月','火','水','木','金','土'],
		weekHeader: '週',
		dateFormat: 'yy/mm/dd',
		firstDay: 0,
		isRTL: false,
		showMonthAfterYear: true,
		yearSuffix: '年',
						<? } ?>
							prevText: ' ',
							nextText: ' ',
							onSelect: function(dateText, inst) {
							setTimeout(function(){setButtonText($(".datepicker").datepicker('getDate').getMonth() + 1);}, 0);
								servcorp.booking.setDate(dateText);
							},
							onChangeMonthYear: function(year, month, inst) {
								setTimeout(function(){setButtonText(month);}, 0);
//								$(".datepicker").datepicker('option','prevText',months[month]);
							}
						});
					});
					
					$('#a_country').on('click','a',function(){ 
						$.mobile.changePage('#select_country', { transition: 'slide' });
					});
					$('#a_location').on('click','a',function(){ 
							$.mobile.changePage('#select_location', { transition: 'slide' });
					});
					$('#a_resource_type').on('click','a',function(){ 
							$.mobile.changePage('#select_resource_type', { transition: 'slide' });
					});
					$('#a_resource').on('click','a',function(){ 
							$.mobile.changePage('#select_resource', { transition: 'slide' });
					});
					$('#a_time').on('click','a',function(){ 
							$.mobile.changePage('#select_time', { transition: 'slide' });
						servcorp.utility.setTimeOptions();});
					$('#a_date').on('click','a',function(){ 
						setTimeout(function(){setButtonText($(".datepicker").datepicker('getDate').getMonth() + 1);}, 0);

						$.mobile.changePage('#select_date', { transition: 'slide' });

					});
					$('#arr_cnt').attr('class','arrow');
					$('#arr_loc').attr('class','arrow');
					$('#arr_rt').attr('class','arrow');
					$('#arr_r').attr('class','arrow');
				}
				$('#done_booking').attr('name','');
				$('#countrylist').html('');
				$('#citylist').html('');
				$('#RTlist').html('');
				$('#Rlist').html('');
				$('#s_hour').off();
				$('#duration').off();
				
				
			},
			setTimeOptions		:	function(getTimeSlot,prev_date) {
				if(getTimeSlot) {
					servcorp.ajax.getTimeSlot(servcorp.booking.date);
					$('#s_hour').change(function(){
						if(!($('#duration').val()))
							$('#duration').val("0:30");
						servcorp.utility.showAvailableTimeSlots();
						<!-- $('#s_hour').parent().hide(); -->
					});
					$('#duration').change(function(){
						servcorp.utility.showAvailableTimeSlots();
					});
				}
				if(typeof(prev_date) == "undefined") {
					$('#time_suggest').html('');
					$('#s_hour').html('');
					var curr = new Date();
					var hr=curr.getHours();
					var min=curr.getMinutes();
					$('#duration').html('<option value=""></option>');
					$('#s_hour').html('<option value=""></option>');
					if(servcorp.utility.countrycode!='HK')
					for(h=8;h<18;h++) {
						h1=((h<10)? '0'+h:h);
						for(m=0;m<60;m+=10) {
							mi=((m<10)? '0'+m:m);
							if((h1==17 && m>30) || (h1==8 && m<30)) continue;
	 						$('#s_hour').append('<option value="'+h1+':'+mi+'">'+h+':'+mi+'</option>');
						}
					}
					else
						for(h=9;h<18;h++) {
							h1=((h<10)? '0'+h:h);
							for(m=0;m<60;m+=10) {
								mi=((m<10)? '0'+m:m);
								if((h1==18 && m>00) || (h1==9 && m<00)) continue;
		 						$('#s_hour').append('<option value="'+h1+':'+mi+'">'+h+':'+mi+'</option>');
							}
						}
					for(h=0;h<=9;h++) {
						for(m=0;m<60;m+=10) {
							if((m==0 && h==0)||(0 < h && (m!=30 && m!=0) )) continue;
							$('#duration').append('<option value="'+h+':'+m+'">'+((h!=0)? h+((h==1)?' hr':' hrs'):'')+' '+((m!=0)? m+' mins':'')+'</option>');
							if(h==9) break;
						}
					}
				}
			},
			
			showAvailableTimeSlots	:	function() {
				var hour = $('#s_hour').val();
				var duration = $('#duration').val();
				var s_hr = Number(hour.split(':')[0]);
				var s_min = Number(hour.split(':')[1]);
				var e_hr = s_hr+Number(duration.split(':')[0]);
				var e_min = s_min+Number(duration.split(':')[1]);
				if(e_min==60) {e_hr+=1;e_min=0;}
				else if(e_min>60) {e_hr+=1;e_min=e_min-60;}
				if(!hour) 
					$('#time_suggest').html('<label><?=$translater->translate("Select a start time")?></label>');
				else if(!duration)
					$('#time_suggest').html('<label><?=$translater->translate("Select a duration")?></label>');
				else {
					var avail=this.isTimeAvail(s_hr,s_min,e_hr,e_min);
					var suggest_slots=[];
					var after_hour=false;
					if(!avail) {
						suggest_slots=this.timeSlotSuggestions(e_hr,e_min,duration);
					}
					if(servcorp.utility.countrycode!='HK')
						{if((e_hr>17) || (e_hr==17 && e_min>30)){ avail=false; after_hour=true;}}
					else
						{if((e_hr>18) || (e_hr==18 && e_min>00)){ avail=false; after_hour=true;}}
					if(avail) {
						$('#time_suggest').html('<ul class="itemDetails">'+'<li  class="arrow"><a href="javascript:void(0);"'+
								' onclick="servcorp.booking.setTime('+s_hr+','+((s_min==0)?'00':s_min)+','+e_hr+','+((e_min==0)?'00':e_min)+');">'+s_hr+':'
								+((s_min==0)?'00':s_min)+' to '+e_hr+':'+((e_min==0)?'00':e_min)+'</a></li>'+'</ul>');
						after_hour=false;
					} else {
						if(after_hour && suggest_slots.length==0){
							alert('<?=$translater->translate("Please contact the Servcorp Staff for after hour bookings.")?>'+"\n"+
									'<?=$translater->translate("Email us")?>:'+servcorp.utility.email+"\n"+
									'<?=$translater->translate("Call us")?>:'+$('#call').text());
						}
						var htmlStr='<label><?=$translater->translate("Select a time slot")?></label><ul class="itemDetails">'+'<li  class=""><a class="strikeout" href="javascript:void(0);">'
									+s_hr+':'+((s_min==0)?'00':s_min)+' to '+e_hr+':'+((e_min==0)?'00':e_min)+' Not available'+'</a></li></ul>';
						for(var sugg in suggest_slots) {
							var sh=suggest_slots[sugg]['s_hr'];
							var sm=suggest_slots[sugg]['s_min'];
							var eh=suggest_slots[sugg]['e_hr'];
							var em=suggest_slots[sugg]['e_min']
							htmlStr+='<ul class="itemDetails"><li  class="arrow"><a href="javascript:void(0);"'+
							' onclick="servcorp.booking.setTime('+sh+','+((sm==0)?'00':sm)+','+eh+','+((em==0)?'00':em)+');">'+sh+':'
							+((sm==0)?'00':sm)+' to '+eh+':'+((em==0)?'00':em)+'</a></li></ul>';
						}
						if(after_hour || suggest_slots.length==0)
						$('#time_suggest').html('<ul class="itemDetails">'+'<li  class=""><a class="strikeout" href="javascript:void(0);">'
								+s_hr+':'+((s_min==0)?'00':s_min)+' to '+e_hr+':'+((e_min==0)?'00':e_min)+' Not available'+'</a></li></ul>');
						else
						$('#time_suggest').html(htmlStr);
					}
				}
			},
			
			isTimeAvail		:	function(s_hr,s_min,e_hr,e_min) {
				st_sel=new Date('1/1/2011 '+s_hr+':'+s_min+':00');
				et_sel=new Date('1/1/2011 '+e_hr+':'+e_min+':00');
				var ret=true;
				for(var slot in this.busyslots){
					st_get=new Date('1/1/2011 '+this.busyslots[slot]['StartHour']+':'+this.busyslots[slot]['StartMin']+':00');
					et_get=new Date('1/1/2011 '+this.busyslots[slot]['EndHour']+':'+this.busyslots[slot]['EndMin']+':00');
					if((st_sel>=st_get && st_sel<et_get) || (et_sel>st_get && et_sel<=et_get) || (st_sel<st_get && et_sel>et_get)) return false;
				}
				return ret;
			},
			
			timeSlotSuggestions	:	function(e_hr,e_min,duration) {
				var sugg_list=[];
				var s_hr='',s_min='',e_hr=e_hr,e_min=e_min;
				while(sugg_list.length!=3) {
					s_hr = e_hr;
					s_min = e_min;
					e_hr = s_hr+Number(duration.split(':')[0]);
					e_min = s_min+Number(duration.split(':')[1]);
					if(e_min==60) {e_hr+=1;e_min=0;}
					else if(e_min>60) {e_hr+=1;e_min=e_min-60;}
					if((e_hr>17) || (e_hr==17 && e_min>30)) break;
					if(this.isTimeAvail(s_hr,s_min,e_hr,e_min)) sugg_list.push({'s_hr':s_hr,'s_min':s_min,'e_hr':e_hr,'e_min':e_min});
				}
				return sugg_list;
			},
			
			findDuration	:	function(stime,etime) {
				var s = new Date('1/1/2010 '+stime+':00');
				var e = new Date('1/1/2010 '+etime+':00');
				var timediff=e.getTime()-s.getTime();
				var weeks = Math.floor(timediff / (1000 * 60 * 60 * 24 * 7));
				timediff -= weeks * (1000 * 60 * 60 * 24 * 7);
				var days = Math.floor(timediff / (1000 * 60 * 60 * 24)); 
				timediff -= days * (1000 * 60 * 60 * 24);
				var hours = Math.floor(timediff / (1000 * 60 * 60)); 
				timediff -= hours * (1000 * 60 * 60);
				var mins = Math.floor(timediff / (1000 * 60)); 
				timediff -= mins * (1000 * 60);
				return hours+':'+mins;
			},
			setEmailPhone	:function(getEmailPhoneForDate){
				if(getEmailPhoneForDate)
					servcorp.ajax.getEmailPhoneForDate();
			},
			
			reset	:	function() {
				alert('<?=$translater->translate("Oops! We were unable to connect to the server. Please try again.")?>');	
				window.location.reload();
			}
		}
	})();
})();

function splBack(){
	//$('#select_country').css('display', 'block');
	console.log('splback');
		$.mobile.changePage('#select_country', { transition: 'slide' });
}
