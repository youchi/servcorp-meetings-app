define( [ "jquery", "./jquery.mobile.core" ], function( $ ) {
(function( $, window, undefined ) {

	var createHandler = function( sequential ){

		if( sequential === undefined ){
			sequential = true;
		}

		return function( name, reverse, $to, $from ) {

			var deferred = new $.Deferred(),
			reverseClass = reverse ? " reverse" : "",
			active	= $.mobile.urlHistory.getActive(),
			toScroll = active.lastScroll || $.mobile.defaultHomeScroll,
			screenHeight = $.mobile.getScreenHeight(),
			maxTransitionOverride = $.mobile.maxTransitionWidth !== false && $( window ).width() > $.mobile.maxTransitionWidth,
			none = !$.support.cssTransitions || maxTransitionOverride || !name || name === "none",
			toPreClass = " ui-page-pre-in",
			toggleViewportClass = function(){
				$.mobile.pageContainer.toggleClass( "ui-mobile-viewport-transitioning viewport-" + name );
			},
			scrollPage = function(){
				$.event.special.scrollstart.enabled = false;

				window.scrollTo( 0, toScroll );

				setTimeout(function() {
					$.event.special.scrollstart.enabled = true;
				}, 150 );
			},
			cleanFrom = function(){
				$from
				.removeClass( $.mobile.activePageClass + " out in reverse " + name )
				.height( "" );
			},
			startOut = function(){
				if( !sequential ){
					doneOut();
				}
				else {
					$from.animationComplete( doneOut );
				}

				$from
				.height( screenHeight + $(window ).scrollTop() )
				.addClass( name + " out" + reverseClass );
			},

			doneOut = function() {

				if ( $from && sequential ) {
					cleanFrom();
				}

				startIn();
			},

			startIn = function(){

				$to.addClass( $.mobile.activePageClass + toPreClass );

				$.mobile.focusPage( $to );

				$to.height( screenHeight + toScroll );

				scrollPage();

				if( !none ){
					$to.animationComplete( doneIn );
				}

				$to
				.removeClass( toPreClass )
				.addClass( name + " in" + reverseClass );

				if( none ){
					doneIn();
				}

			},

			doneIn = function() {

				if ( !sequential ) {

					if( $from ){
						cleanFrom();
					}
				}

				$to
				.removeClass( "out in reverse " + name )
				.height( "" );

				toggleViewportClass();

				if( $( window ).scrollTop() !== toScroll ){
					scrollPage();
				}

				deferred.resolve( name, reverse, $to, $from, true );
			};

			toggleViewportClass();

			if ( $from && !none ) {
				startOut();
			}
			else {
				doneOut();
			}

			return deferred.promise();
		};
	}

	var sequentialHandler = createHandler(),
	simultaneousHandler = createHandler( false );

	$.mobile.defaultTransitionHandler = sequentialHandler;

	$.mobile.transitionHandlers = {
	"default": $.mobile.defaultTransitionHandler,
	"sequential": sequentialHandler,
	"simultaneous": simultaneousHandler
	};

	$.mobile.transitionFallbacks = {};

})( jQuery, this );
});
