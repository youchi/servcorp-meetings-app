<?php
require_once 'Zend/Application.php';
 
class BookingControllerTest extends Zend_Test_PHPUnit_ControllerTestCase{

	public function setUp()
	{
		// Assign and instantiate in one step:
		$this->bootstrap = new Zend_Application(
        	'testing',
			APPLICATION_PATH . '/configs/application.ini'
		);
		
		parent::setUp();
	}
	
	public function tearDown()
    {
        //echo 'Tearing down!';
    }
	
    public function callModelsBooking($params) {
    	$soap = new Model_SOAPCalls();
    	return $soap->createBooking($params);
    }
    
    public function _testValidBooking()
    {
    	/*$dateCreated = Date('Y-m-d h:i:s');
		$dataset = array('username'=>'pollenizer','ResourceID'=>12,'Comments'=>'Global Warming!','BookingDate'=>'2010-07-06',
						'StartTime'=>'2:00 pm','EndTime'=>'5:00 pm','DurationUnit'=>2,'ComplimentUnit'=>1,'CreatedBy'=>'nithin',
						'DateCreated'=>$dateCreated,'Updatedby'=>'pollenizer','LastUpdated'=>$dateCreated,'ClientID'=>'12','SiteID'=>'http://google.com',
						'WeekEnd'=>false);*/
    	$booking_param=array('UserName'=>'pollenizer','ResourceID'=>17,
								 'SelectedDate'=>'20100925','StartHour'=>10,'StartMin'=>30,'EndHour'=>11,'EndMin'=>30,'Comments'=>'','Invitees'=>'');
    	$dataset = 	   array('UserName'=>'pollenizer','ResourceID'=>17,
    							'SelectedDate'=>'20100921','StartHour'=>10,'StartMin'=>30,'EndHour'=>11,'EndMin'=>30,'Comments'=>'Global Warming!','Invitees'=>'shanmuga@pollenizer.in');
//		print_r($this->callModelsBooking($booking_param));
    }    
    
	public function testGetLocationFromCountry() {
		$mod = new Model_Location();
//		$mod->getLocationFromCountry('AU')->toArray();
	}
	
	public function testDeleteBooking() {
		$mod = new Model_SOAPCalls();
//		print_r($mod->doCancelBooking('pollenizer', 268330));
	}
	
	public function testUpdateBooking() {
		$soapObj  = new Model_SOAPCalls();
		$data = array('RefNo'=>268336,'EditDate'=>20101115,'StartHour'=>14,'StartMin'=>30,
						'EndHour'=>15,'EndMin'=>30,'Comments'=>'','Invitees'=>'');
		$res = $soapObj->doUpdateBooking($data);
		print_r($res);
	}
	
}

class BookingResourceTest extends Zend_Test_PHPUnit_ControllerTestCase
{
	public function setUp()
	{
		$this->bootstrap=new Zend_Application('testing',APPLICATION_PATH.'/configs/application.ini');
		parent::setUp();
	}
	
	public function teardown()
	{
		//echo 'tearing down';
	}
	
	public function callModelsgetBookingResource($username, $datefilterval)
	{
		/*$soap=new Model_SOAPCalls();
		return $this->getBookingResource($username, $datefilterval);*/
	}
	
	
	
	/*public function testValidBookingResource()
	{
		$this->assertEquals(true,$this->callModelsgetBookingResource('pollenizer', 'today'));
	}
	
	public function testEmptyBookingResource()
	{
			
		$this->assertEquals(false,$this->callModelsgetBookingResource('', ''));
	}
	
	public function  testInvalidBookingResource($username,$datefilterval)
	{
	 	$this->assertEquals(false,$this->callModelsgetBookingResource('xminds','today'));
	}
	
	public function testGetResource()
	{
		$this->assertEquals(true,$this->callModelsgetBookingResource('pollenizer','today'));
	}
	
	public function testGetStartTime()
	{
		$this->assertEquals(true,$this->callModelsgetBookingResource('pollenizer','today'));
	}
	
	public function testGetBookingDate()
	{
		$this->assertEquals(true,$this->callModelsgetBookingResource('pollenizer', 'today'));
	}*/
}
