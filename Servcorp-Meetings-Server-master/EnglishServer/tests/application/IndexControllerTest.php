<?php
require_once 'Zend/Application.php';
 
class IndexControllerTest extends Zend_Test_PHPUnit_ControllerTestCase{

	public function setUp()
	{
		// Assign and instantiate in one step:
		$this->bootstrap = new Zend_Application(
        	'testing',
			APPLICATION_PATH . '/configs/application.ini'
		);
		parent::setUp();
	}
	
	public function tearDown()
    {
        //echo 'Tearing down!';
    }
		
    public function testHomeIsOnIndexAction()
    {
        $this->dispatch('/');
        $this->assertController('index');
        $this->assertAction('index');
    }    

}
