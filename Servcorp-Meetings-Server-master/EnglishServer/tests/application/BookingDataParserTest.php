<?php
require_once 'Zend/Application.php';

class BookingDataParserTest extends Zend_Test_PHPUnit_ControllerTestCase{

	protected $baseurl;
	protected $bookingXml;
	public function setUp()
	{
		// Assign and instantiate in one step:
		Zend_Registry::set('soapOptions', array("login"=>'mobile_service',"password"=>'!p0ll3rn1z3r!p4ss'));
		$this->bootstrap = new Zend_Application(
        	'testing',
		APPLICATION_PATH . '/configs/application.ini'
		);
		parent::setUp();
		$this->baseurl = 'http://servcorp.localhost/';
		$this->bookingXml = new UTILS_BookingDataParser($this->baseurl);
	}

	public function tearDown()
	{
		//echo 'Tearing down!';
	}

	public function _testBookingData()
	{
		$m = new Model_Country();
		$c = $m->getCountry();
		$cc = new Model_City();
		$cc->getCity();

		$lo = new Model_Location();
		$lo->getLocationFromCountry('AU')->toArray();

		$rt = new Model_ResourceType();
		$rt->getResourceTypeFromLocation('ADE')->toArray();

		$r = new Model_Resource();
		$r->getRNameFromLoc(1, 'ADE')->toArray();
	}
	public function testInsertCountry(){
		$this->bookingXml->insertCountry();
	}
	public function testInsertCity(){
		$this->bookingXml->insertCity();
	}
	public function testInsertLocation(){
		$this->bookingXml->insertLocation();
	}
	public function testInsertResource(){
		$this->bookingXml->insertResource();
	}
	public function _testInsertResourceType(){
		$this->bookingXml->insertResourceType();
	}
	public function _testInsertSite(){
		$this->bookingXml->insertSite();
	}
	public function _testInsertHoliday(){
		$this->bookingXml->insertHoliday();
	}
	public function  _testInsertWeekly(){
		$this->bookingXml->insertWeekly();
	}
	public function  _testinserttblHottdeskService(){
		$this->bookingXml->inserttblHottdeskService();
	}
	public function  _testinserttblHottdeskServiceAvailability(){
		$this->bookingXml->inserttblHottdeskServiceAvailability();
	}
	public function  _testinserttblLanguage(){
		$this->bookingXml->inserttblLanguage();
	}
	public function  _testinserttblLocationPhoneInfo(){
		$this->bookingXml->inserttblLocationPhoneInfo();
	}
	public function  _testinserttblOfficePortal(){
		$this->bookingXml->inserttblOfficePortal();
	}
	public function  _testinserttblTaskTimerGroup(){
		$this->bookingXml->inserttblTaskTimerGroup();
	}
	public function  _testinsertview_HottdeskService(){
		$this->bookingXml->insertview_HottdeskService();
	}
}