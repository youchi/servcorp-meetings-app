<?php
require_once '../application/models/SOAPCalls.php';
class SOAPAuth implements Zend_Auth_Adapter_Interface{
	
	protected $_uname;
	protected $_passwd;
	
	public function __construct($username,$password) {
		$this->_uname = $username;
		$this->_passwd = $password;
	}
	
	public function authenticate() {
		$soap_obj = new Model_SOAPCalls();
		if($soap_obj->checkLogin($this->_uname, $this->_passwd)) {
			return new Zend_Auth_Result(Zend_Auth_Result::SUCCESS, $this->_uname, array());
		}
		return new Zend_Auth_Result(Zend_Auth_Result::FAILURE_CREDENTIAL_INVALID, $this->_uname);
	}
}